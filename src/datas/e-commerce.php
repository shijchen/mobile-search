<?php
/**
 * 电商,团购,购物相关
 */

return [
	'gome'=>[
		'parser'=>['string', 'register', ['regexp', "#true\:.*?\:showValidateCode#"]],
		'desc'=>'国美',
		
		'match'=>[
			'url'=>'https://reg.gome.com.cn/register/index/person',
			'data'=>'',
			'http_header'=>['Upgrade-Insecure-Requests:1'],
			'attributes'=>['pneg'=>"#buryImage\/(\d+)\.pneg#"]
		],
		
		'list'=> [
			[
				'url'=>'https://reg.gome.com.cn/images/buryImage/{$pneg}.pneg',
				'data'=>''
			]
		],
		
		'main'=> [
			'url'=>'http://reg.gome.com.cn/register/validateExist/refuse.do',
			'post'=>true,
			'referer'=>'https://reg.gome.com.cn/register/index/person',
			'data'=>'login={$mobile}',
		]
	],
	
	'jd'=>[
		'parser'=>['json', 'register', ['success', 1]],
		'desc'=>'京东',
		
		'list'=> [
			[
				'url'=>'https://reg.jd.com/reg/person',
				'data'=>''
			]
		],
		
		'main'=> [
			'url'=>'https://reg.jd.com/validateuser/isMobileEngaged',
			'data'=>'phone=+0086{$mobile}&mobile=+0086{$mobile}&_=1477887188868',
		]
	],
	
	'meituan'=>[
		'parser'=>['json', 'register', ['error.code', 101066]],
		'desc'=>'美团',
		
		'match'=> [
			'url'=> 'https://passport.meituan.com/account/unitivesignup',
			'data'=> 'service=www',
			'attributes'=> ['csrf'=>"#id\=\"csrf\".*?\>(.*?)\<#"],
		],
		
		'main'=> [
			'url'=>'https://passport.meituan.com/account/signupcheck',
			'post'=>true,
			'data'=>'mobile={$mobile}',
			'http_header'=> [
				'X-Client:javascript',
				'X-CSRF-Token:{$csrf}'
			]
		]
	],
	
	'dangdang'=>[
		'parser'=>['string', 'register', ['str', 'true']],
		'desc'=>'当当',
		
		'list'=> [
			[
				'url'=>'https://login.dangdang.com/Register.aspx',
				'data'=>''
			]
		],
		
		'main'=> [
			'url'=>'https://login.dangdang.com/p/mobile_checker.php',
			'post'=>true,
			'data'=>'mobile={$mobile}',
		]
	],
	
	'tootoo'=>[
		'parser'=>['json', 'register', ['Result.Data.loginNameCheckResult', 100502]],
		'desc'=>'沱沱工社',
		
		'main'=> [
			'url'=>'http://sapi.tootoo.cn/authorize/MainServlet',
			'data'=>'req_fmt_type=jsonp&method=checkRegisterInput&req_str={"loginName":"{$mobile}","nickName":null,"loginNameType":"0","scope":"11101"}&callback=jQuery111206298870088240098',
		]
	],
	
	'lashou'=>[
		'parser'=>['string', 'register', ['str', '0']],
		'desc'=>'拉手网',
		
		'main'=> [
			'url'=>'http://www.lashou.com/account/detect/mobile/',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'womai'=>[
		'parser'=>['string', 'register', ['str', 'true']],
		'desc'=>'我买网',

		'main'=> [
			'url'=>'https://passport.womai.com/register/judgephone.do',
			'data'=>'Phone={$mobile}',
			'post'=>true,
		]
	],
	
	'elong'=>[
		'parser'=>['json', 'register', ['value', 1]],
		'desc'=>'艺龙',

		'main'=> [
			'url'=>'https://secure.elong.com/passport/isajax/Register/ValidateMobileOrEmail',
			'data'=>'mobile={$mobile}&language=cn&viewpath=~%2Fviews%2Fmyelong%2Fpassport%2FRegister.aspx&rnd=20161031115210',
		]
	],
	
	'gree'=>[
		'parser'=>['json', 'register', ['status', 1]],
		'desc'=>'格力',
		
		'main'=> [
			'url'=>'https://mall.gree.com/mall/CheckLogonIdCmd',
			'data'=>'logonId={$mobile}&storeId=10651&catalogId=10001&langId=-7',
			'post'=>true
		]
	],
	
	'ehaier'=>[
		'parser'=>['json', 'register', ['success', false]],
		'desc'=>'海尔',
		
		'main'=> [
			'url'=>'http://member.ehaier.com/checkUserNameExist.html',
			'data'=>'userName={$mobile}&random=0.948199500066746',
		]
	],
	
	'xiaomi'=>[
		'parser'=>['json', 'register', ['code', 70016]],
		'desc'=>'小米',
		
		'match'=> [
			'url'=> 'https://account.xiaomi.com/pass/serviceLogin',
			'data'=> '',
			'attributes'=> ['_sign'=>"#\"_sign\"\:\"(.*?)\"#"],
		],

		'main'=> [
			'url'=>'https://account.xiaomi.com/pass/serviceLoginAuth2?_dc=1477895885649',
			'data'=>'_json=true&callback=https://account.xiaomi.com&sid=passport&qs='.urlencode('%3Fsid%3Dpassport').'&_sign={$_sign}&serviceParam={"checkSafePhone":false}&user={$mobile}&hash=7BF7C039B4A97210F202FCA79E8DF29F',
			'post'=>true,
		]
	],
	
	'oppo'=>[
		'parser'=>['json', 'register', ['resultCode', 3008]],
		'desc'=>'OPPO',
		
		'main'=> [
			'url'=>'http://id.oppo.com/login?type=1',
			'data'=>'{"username":"{$mobile}","password":"d5c751fb1321ff55defc7ccf1a394e11","type":"1","style":"2","source":"http%3A%2F%2Fwww.oppo.com%2Fcn%2F"}',
			'post'=>true,
			'http_header'=>['Content-Type:application/json; charset=UTF-8']
		]
	],
	
	'le'=>[
		'parser'=>['json', 'register', ['bean.result.status', 1]],
		'desc'=>'乐视',
		
		'main'=> [
			'url'=>'https://sso.le.com/user/checkMobileExists/mobile/{$mobile}',
			'data'=>'',
		]
	],
	
	'dianping'=>[
		'parser'=>['json', 'register', ['msg.err', '用户名或密码错误！(105)']],
		'desc'=>'大众点评',
		
		'match'=>[
			'url'=>'https://www.dianping.com/account/ajax/captchaShow',
			'data'=>'captchaChannel=201&params={"username":"{$mobile}"}',
			'post'=>true,
			'attributes'=> ['uuid'=>"#uuid\"\:\"(.*?)\"#"],
		],
		
		'main'=> [
			'url'=>'https://www.dianping.com/account/ajax/passwordLogin',
			'data'=>'username={$mobile}&password=ZAqX1125456ffz&uuid={$uuid}&keepLogin=on',
			'post'=>true,
		]
	],
	
	'flyme'=>[
		'parser'=>['json', 'register', ['value', false]],
		'desc'=>'魅族',
		
		'main'=> [
			'url'=>'https://i.flyme.cn/uc/system/webjsp/member/isValidPhone',
			'data'=>'phone=0086%3A{$mobile}',
		]
	],
	
	'smartisan'=>[
		'parser'=>['json', 'register', ['errno', 1206]],
		'desc'=>'锤子手机',
		
		'main'=> [
			'url'=>'https://account.smartisan.com/v2/cellphone/?m=get&cellphone=%2B86%20{$mobile}&action=check',
			'data'=>'',
			'post'=>true,
			'referer'=>'https://account.smartisan.com/',
		]
	],
	
	'vmall'=>[
		'parser'=>['json', 'register', ['existAccountFlag', 1]],
		'desc'=>'华为商城',
		
		'match'=> [
			'url'=> 'https://hwid1.vmall.com/CAS/portal/userRegister/regbyphone.html',
			'data'=> '',
			'attributes'=> ['pageToken'=>"#pageToken\:\"(.*?)\",#"],
		],
		
		'main'=> [
			'url'=>'https://hwid1.vmall.com/CAS/ajaxHandler/isExsitUser?random=1477910766490',
			'data'=>'userAccount=0086{$mobile}&pageToken={$pageToken}',
			'post'=>true,
		]
	],
	
	'vivo'=>[
		'parser'=>['json', 'register', ['stat', 400]],
		'desc'=>'vivo',
		
		'main'=> [
			'url'=>'https://passport.vivo.com.cn/v3/web/checkRegAccount',
			'data'=>'account={$mobile}&type=0',
			'post'=>true,
		]
	],
	
	'myzte'=>[
		'parser'=>['json', 'register', ['error', "该手机号已被注册，请更换一个"]],
		'desc'=>'中兴手机',
		
		'main'=> [
			'url'=>'https://www.myzte.com/passport-check_login_name.html',
			'data'=>'pam_account[login_name]={$mobile}',
			'post'=>true,
			'mobile'=>false
		]
	],
	
	'newegg'=>[
		'parser'=>['json', 'register', ['Type', 2]],
		'desc'=>'新蛋',
		
		'main'=> [
			'url'=>'https://secure.newegg.cn/Ajax/Customer/CheckCustomerInfo.aspx?radom0.23461473248870646',
			'data'=>'Phone={$mobile}&Email=&Type=1',
			'post'=>true,
		]
	],
	
	'wangfujing'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'王府井商城',
		
		'main'=> [
			'url'=>'https://www.wangfujing.com/webapp/wcs/stores/servlet/WFJCheckLogonIDUnique',
			'data'=>'catalogId=10101&storeId=10154&logonId={$mobile}',
		]
	],
	
	'lenovo'=>[
		'parser'=>['string', 'register', ['str', 'response.authAccount("1")']],
		'desc'=>'联想',
		
		'main'=> [
			'url'=>'http://reg.lenovo.com.cn/auth/validateAccount',
			'data'=>'account={$mobile}&callback=response.authAccount&_=1478171654958	',
		]
	],
	'midea'=>[
		'parser'=>['string', 'register', ['regexp', "#\"errcode\"\s*\:\s*(539299908|539299910)\,#"]],
		'desc'=>'美的',

		'match'=>[
			'url'=>'https://w.midea.com/mlogin/get_nonce_id',
			'data'=>'',
			'attributes'=>['nonceid'=>"#\"strNonceId\"\:\"(.*?)\"#"]
		],
		
		'main'=> [
			'url'=>'https://w.midea.com/mlogin/login_with_passwd',
			'data'=>'mobile={$mobile}&passwd=18f6bbf48ed4564bf0d0e985adf2319f&nonceid={$nonceid}',
			'post'=>true
		]
	],
	
	'moonbasa'=>[
		'parser'=>['json', 'register', ['Result', '-1']],
		'desc'=>'梦芭莎',
		
		'main'=> [
			'url'=>'https://login.moonbasa.com/home/checkmobile',
			'data'=>'id={$mobile}',
			'post'=>true,
		]
	],
	
	'manpianyi'=>[
		'parser'=>['string', 'register', ['regexp', "#已注册#"]],
		'desc'=>'蛮便宜',
		
		'main'=> [
			'url'=>'http://home.manpianyi.com/servet/check_mobile.php',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'juanpi'=>[
		'parser'=>['json', 'register', ['status', 1004]],
		'desc'=>'卷皮网',
		
		'main'=> [
			'url'=>'https://user.juanpi.com/register/checkMobile',
			'data'=>'email={$mobile}',
			'post'=>true
		]
	],
];