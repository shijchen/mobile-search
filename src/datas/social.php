<?php
/**
 * 社交,社区,婚恋相关
 */

return [
	'tianya'=>[
		'parser'=>['json', 'register', ['code', 4]],
		'desc'=>'天涯',
		
		'main'=> [
			'url'=>'https://passport.tianya.cn/register/checkName.do',
			'data'=>'userName={$mobile}&_r=1478593477581&__sid=0%231%231.0%2308fe1d91-7cf9-4a38-861f-5c24ab3a8799',
		]
	],
	
	'renren'=>[
		'parser'=>['string', 'register', ['str', '手机号已经绑定，不能注册']],
		'desc'=>'人人网',
		
		'main'=> [
			'url'=>'http://reg.renren.com/AjaxRegisterAuth.do',
			'data'=>'authType=email&stage=3&t=1477899450326&value={$mobile}&requestToken=&_rtk={$rand}',
			'post'=>true,
		]
	],
	
	'jiayuan'=>[
		'parser'=>['string', 'register', ['regexp', "#\<cmd.*?show\s+null.*?cmd\>#"]],
		'desc'=>'世纪佳缘',
		
		'main'=> [
			'url'=>'http://reg.jiayuan.com/libs/xajax/reguser.server.php?processUserMobile',
			'data'=>'xajax=processUserMobile&xajaxargs%5B%5D=%3Cxjxquery%3E%3Cq%3Emobile%3D{$mobile}%3C%2Fq%3E%3C%2Fxjxquery%3E&xajaxr=1477900283196',
			'post'=>true,
			'http_header'=>['sid: 11640d']
		]
	],
	
	'kaixin001'=>[
		'parser'=>['string', 'register', ['str', '10']],
		'desc'=>'开心网',
		
		'main'=> [
			'url'=>'http://www.kaixin001.com/interface/checknickname.php',
			'data'=>'rg=mobile&nickname={$mobile}',
		]
	],
	
	'zhenai'=>[
		'parser'=>['string', 'register', ['str', 'exist']],
		'desc'=>'珍爱网',
		
		'main'=> [
			'url'=>'http://register.zhenai.com/register/validateMobile3.jsps',
			'data'=>'mobile={$mobile}&d=0.34828087372211725',
		]
	],
	
	'baihe'=>[
		'parser'=>['json', 'register', ['state', 0]],
		'desc'=>'百合网',
		
		'main'=> [
			'url'=>'http://my.baihe.com/register/emailCheckForXs',
			'data'=>'jsonCallBack=jq&l1478076894839&email={$mobile}&_=1478076894855',
		]
	],
	
	'wed114'=>[
		'parser'=>['json', 'register', ['status', 0]],
		'desc'=>'wed114',
		
		'main'=> [
			'url'=>'http://passport.wed114.cn/index.php?m=User&a=checkMobile',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'xiaoliangkou'=>[
		'parser'=>['json', 'register', ['valid', false]],
		'desc'=>'小两口',
		
		'main'=> [
			'url'=>'http://www.xiaoliangkou.com/check/mobile/{$mobile}',
			'data'=>'',
		]
	],
	
	'mop'=>[
		'parser'=>['json', 'register', ['error', "用户名或密码错误"]],
		'desc'=>'猫扑',
		
		'main'=> [
			'url'=>'http://passport.mop.com/ajax/userLogin',
			'data'=>'loginName={$mobile}&loginPasswd=X{$time}&autoLogin=1&imageCode=&callback=jsonp2',
		]
	],
	
	'budejie'=>[
		'parser'=>['json', 'register', ['code', 1007]],
		'desc'=>'百思不得姐',
		
		'main'=> [
			'url'=>'http://d.api.budejie.com/user/login',
			'data'=>'phonenum={$mobile}&password=60bfc3bff7af27775b28deed4c3742bx&callback=cb&_=1478513966559',
		]
	],
	
	'yiban'=>[
		'parser'=>['json', 'register', ['code', 201]],
		'desc'=>'易班',
		
		'main'=> [
			'url'=>'http://www.yiban.cn/user/reg/checkMobileAjax',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'zhihu'=>[
		'parser'=>['json', 'register', ['errcode', 100005]],
		'desc'=>'知乎',
		
		'main'=> [
			'url'=>'https://www.zhihu.com/login/phone_num',
			'data'=>'password=1q12345OLKMBH7&captcha=dtpp&remember_me=true&phone_num={$mobile}',
			'post'=>true,
		]
	],
];