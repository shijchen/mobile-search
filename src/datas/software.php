<?php
/**
 * 软件
 */
 
return [
	'wandoujia'=>[
		'parser'=>['json', 'register', ['error', 1010]],
		'desc'=>'豌豆荚',

		'main'=> [
			'url'=>'https://account.wandoujia.com/v4/api/login',
			'data'=>'username={$mobile}&password=DDD111111sssbvx&seccode=&from=web&source=web,www.wandoujia.com&lang=zh',
			'post'=>true,
		]
	],
	
	'meitu'=>[
		'parser'=>['json', 'register', ['error_code', 20015]],
		'desc'=>'美图秀秀',
		
		'main'=> [
			'url'=>'http://id.meitu.com/users/login',
			'data'=>'username={$mobile}&password=X{$time}&remember_me=0',
			'post'=>true
		]
	],
	
	'mumayi'=>[
		'parser'=>['json', 'register', ['row', 2]],
		'desc'=>'木蚂蚁',
		
		'main'=> [
			'url'=>'http://u.mumayi.com/?a=login',
			'data'=>'username={$mobile}&userpass=msdks{$mobile}',
			'post'=>true,
		]
	],
	
	'anzhi'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'安智',
		
		'main'=> [
			'url'=>'http://i.anzhi.com/web/account/check-telephone',
			'data'=>'telephone={$mobile}',
		]
	],
	
	'uc'=>[
		'parser'=>['json', 'register', ['status', 20180001]],
		'desc'=>'UC',
		
		'main'=> [
			'url'=>'https://api.open.uc.cn/cas/ajax/checkMobile',
			'data'=>'registerName={$mobile}&uc_param_str=nieisivefrpfbimilaprligiwiut&display=pc&t=1478526704987&browser_type=&client_id=106',
		]
	],
	
	'mgyun'=>[
		'parser'=>['json', 'register', ['msg', 1]],
		'desc'=>'刷机大师',
		
		'main'=> [
			'url'=>'http://passport.mgyun.com/Register/CheckUserPhone',
			'data'=>'num=0.8177221787800082&phone={$mobile}',
		]
	],
	
	'meipai'=>[
		'parser'=>['json', 'register', ['error_code', 21402]],
		'desc'=>'美拍',
		
		'main'=> [
			'url'=>'http://www.meipai.com/users/login',
			'data'=>'phone={$mobile}&phone_flag=86&password=X{$time}&auto_login=true',
			'post'=>true,
			'mobile'=>false,
		]
	],
	
	'jdbbx'=>[
		'parser'=>['string', 'register', ['str', '103']],
		'desc'=>'简单百宝箱',
		
		'main'=> [
			'url'=>'http://user.jdbbx.com/Register.aspx',
			'data'=>'Action=DoJudgeUserName&UserName={$mobile}',
			'post'=>true,
		]
	],
];
