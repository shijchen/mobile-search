<?php
/**
 * 游戏相关
 */

return [
	'66rpg'=>[
		'parser'=>['json', 'register', ['status', -15]],
		'desc'=>'橙光',
		
		'main'=> [
			'url'=>'http://passport.66rpg.com/api/findpwd?Fri%20Nov%2011%202016%2016:54:31%20GMT+0800%20(%E4%B8%AD%E5%9B%BD%E6%A0%87%E5%87%86%E6%97%B6%E9%97%B4)',
			'data'=>'act=userCheck&userName={$mobile}',
			'post'=>true,
		]
	],
	
	'wanmei'=>[
		'parser'=>['json', 'register', ['code', 2]],
		'desc'=>'完美',
		
		'main'=> [
			'url'=>'http://passport.wanmei.com/reg/checkuser',
			'data'=>'username={$mobile}',
			'post'=>true,
		]
	],
	
	'ztgame'=>[
		'parser'=>['string', 'register', ['str', '-1|']],
		'desc'=>'巨人网络',
		
		'list'=>[
			[
				'url'=>'http://reg.ztgame.com/registe/reg_phone.jsp',
				'data'=>'source=giant_site'
			],
		],
		
		'main'=> [
			'url'=>'http://reg.ztgame.com/registe/mobilePhoneRegister',
			'data'=>'type=isBindPhoneNum&phoneNum={$mobile}',
			'post'=>true,
		]
	],
	
	'wangyuan'=>[
		'parser'=>['json', 'register', ['hasuser', 1]],
		'desc'=>'网元圣唐',

		'main'=> [
			'url'=>'https://login.gamebar.com/index/checkphone',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],
	
	'pcgames'=>[
		'parser'=>['json', 'register', ['status', 43]],
		'desc'=>'太平洋游戏',
		
		'main'=> [
			'url'=>'http://passport3.pcgames.com.cn/passport3/api/validate_mobile.jsp?mobile={$mobile}&req_enc=UTF-8',
			'data'=>'{}',
			'post'=>true,
		]
	],
	
	'18183com'=>[
		'parser'=>['json', 'register', ['signupform-login.0', '该邮箱或手机号码已被注册']],
		'desc'=>'18183畅玩',
		
		'match'=>[
			'url'=>'http://passport.18183.com/register',
			'data'=>'',
			'attributes'=>['_csrf'=>"#name\=\"_csrf\"\s*value\=\"(.*?)\"#"]
		],
		
		'main'=> [
			'url'=>'http://passport.18183.com/register',
			'data'=>'_csrf={$_csrf}&SignupForm[login]={$mobile}&ajax=form-signup&SignupForm[password]=&geetest_challenge=&geetest_validate=&geetest_seccode=&SignupForm[mobileCode]=',
			'post'=>true,
		]
	],
	
	'ptbus'=>[
		'parser'=>['json', 'register', ['status', 'ok']],
		'desc'=>'口袋巴士',
		
		'main'=> [
			'url'=>'http://i.ptbus.com/uc_api/checkRegName',
			'data'=>'regname={$mobile}&type=extends',
			'post'=>true,
		]
	],
	
	'youzu'=>[
		'parser'=>['json', 'register', ['RegMobileForm_user_account_m.0', '该手机号已被注册']],
		'desc'=>'游族',
		
		'main'=> [
			'url'=>'http://passport.youzu.com/site/regMobile',
			'data'=>'app_id=&redirect_uri=&state=&response_type=&RegMobileForm%5Buser_account_m%5D={$mobile}&textarea3=&RegMobileForm%5Bverifycode_m%5D=&RegMobileForm%5Buser_password_m%5D=&RegMobileForm%5Buser_password_repeat_m%5D=&RegMobileForm%5Buser_name_m%5D=&RegMobileForm%5Bid_card_m%5D=&RegMobileForm%5Bagree_m%5D=0&RegMobileForm%5Bagree_m%5D=1&ajax=user-mobile-form',
			'post'=>true
		]
	],
	
	'sdo'=>[
		'parser'=>['json', 'register', ['data.recommendLoginType', 15]],
		'desc'=>'盛大',
		
		'main'=> [
			'url'=>'https://cas.sdo.com/authen/checkAccountType.jsonp',
			'data'=>'callback=cb&serviceUrl=register.sdo.com&appId=991002500&areaId=201000&authenSource=2&inputUserId={$mobile}&locale=zh_CN&productId=1&productVersion=1.7&version=21&_=1478086797391',
		]
	],
	
	'61com'=>[
		'parser'=>['json', 'register', ['result', 1103]],
		'desc'=>'淘米游戏',
		
		'main'=> [
			'url'=>'http://account.61.com/login/ajaxLogin',
			'data'=>'uid={$mobile}&pwd=AXx{$time}&login_promot_tag=61',
			'post'=>true
		]
	],
	
	'tgbus'=>[
		'parser'=>['json', 'register', ['code', -2]],
		'desc'=>'电玩巴士',
		
		'main'=> [
			'url'=>'http://user.tgbus.com/api/action.ashx',
			'data'=>'callback=callback9903082364248927&t=checkphone&format=json&geetest_challenge=&geetest_validate=&geetest_seccode=&phone={$mobile}',
			'post'=>true,
		]
	],
	
	'5211game'=>[
		'parser'=>['json', 'register', ['Code', -2]],
		'desc'=>'11平台',
		
		'main'=> [
			'url'=>'http://register.5211game.com/11/api/userexist',
			'data'=>'user={$mobile}',
			'post'=>true,
		]
	],
	
	'xunyou'=>[
		'parser'=>['string', 'register', ['str', '该用户已经存在']],
		'desc'=>'迅游',
		
		'main'=> [
			'url'=>'http://my.xunyou.com/index.php/memberpanel/checkUsserExisted_bak',
			'data'=>'agree_rule=1&loginid={$mobile}&password=&confirm_password=&code=',
			'post'=>true,
		]
	],
	
	'17173com'=>[
		'parser'=>['json', 'register', ['status', 7]],
		'desc'=>'17173',
		
		'main'=> [
			'url'=>'https://passport.17173.com/sso/login',
			'data'=>'v=2&username={$mobile}&password=893c428b6c1915df9d59c50c60ab9e23&validcode=&domain=17173.com&appid=10086&currentUrl=https%3A%2F%2Fpassport.17173.com%2F&isbase64=-1&persistentcookie=0&callback=pprtjsonp4imhzu5gdsbzxgids12trcpe4e',
		]
	],
	
	'd'=>[
		'parser'=>['json', 'register', ['code', 9999]],
		'desc'=>'当乐网',
		
		'main'=> [
			'url'=>'https://oauth.d.cn/auth/checkPhoEmlUsed',
			'data'=>'account={$mobile}',
			'post'=>true,
		]
	],
	
	'gyyx'=>[
		'parser'=>['json', 'register', ['Content', 'false']],
		'desc'=>'光宇游戏',
		
		'main'=> [
			'url'=>'http://reg.gyyx.cn/register/CheckPhoneAccountIsExist',
			'data'=>'jsoncallback=jq&userName={$mobile}&r=0.2859741608935906&_=1478086289144',
		]
	],
	
	'ali213'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'游侠网',
		
		'main'=> [
			'url'=>'http://i.ali213.net/ajax.html',
			'data'=>'action=ajaxidentity&mobile={$mobile}',
		]
	],
	
	'duoyi'=>[
		'parser'=>['string', 'register', ['regexp', "#^[^1234]$#"]],
		'desc'=>'2980/多益',
		
		'match'=>[
			'url'=>'http://www.2980.com/Register.aspx',
			'data'=>'',
			'attributes'=>['accountflag'=>"#id=\"accountflag\"\s+value=\"(.*?)\"#"]
		],
		
		'main'=> [
			'url'=>'http://www.2980.com/ashx/isExistPhone.aspx',
			'data'=>'txtPhone={$mobile}&accountflag={$accountflag}&_=1478168338498',
		]
	],
	
	'locojoy'=>[
		'parser'=>['json', 'register', ['Result', -1]],
		'desc'=>'卓越游戏',
		
		'main'=> [
			'url'=>'http://reg.locojoy.com/AJAX/CheckUserName.ashx',
			'data'=>'username={$mobile}&t=0.43774142924964643&_=1480478318539',
		]
	],
	
	'xiaopi'=>[
		'parser'=>['json', 'register', ['status', '10001']],
		'desc'=>'小皮',
		
		'main'=> [
			'url'=>'http://user.xiaopi.com/reg/ajaxMobileCheck/times/6770/',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
];