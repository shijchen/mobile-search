<?php
/**
 * 医疗,健康等
 */

return [
	'haodf'=>[
		'parser'=>['string', 'register', ['str', 1]],
		'desc'=>'好大夫',
		
		'main'=> [
			'url'=>'https://passport.haodf.com/user/ajaxuseridcnt4name',
			'data'=>'username={$mobile}&type=NORMAL&_=1478590562378',
		]
	],
	
	'39net'=>[
		'parser'=>['string', 'register', ['regexp', "#\"Success\"\:false#"]],
		'desc'=>'39健康网',
		
		'main'=> [
			'url'=>'http://my.39.net/UserService.asmx/CheckPhoneReg',
			'data'=>'phone={$mobile}&pid=0',
			'post'=>true
		]
	],
	
	'jinxiang'=>[
		'parser'=>['json', 'register', ['code', 110003]],
		'desc'=>'金象网',
		
		'main'=> [
			'url'=>'http://login.jinxiang.com/register/checkUsername.do',
			'data'=>'username={$mobile}',
			'post'=>true
		]
	],
	
	'360kad'=>[
		'parser'=>['json', 'register', ['Result', false]],
		'desc'=>'康爱多',
		
		'main'=> [
			'url'=>'http://user.360kad.com/Register/IsExisteMobile',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'111com'=>[
		'parser'=>['string', 'register', ['str', '1']],
		'desc'=>'1药房',
		
		'main'=> [
			'url'=>'http://passport.111.com.cn/sso/customer/validateEmail.action',
			'data'=>'email={$mobile}',
			'post'=>true
		]
	],
	
	'bjguahao'=>[
		'parser'=>['json', 'register', ['code', 2010]],
		'desc'=>'北京挂号',
		
		'main'=> [
			'url'=>'http://www.bjguahao.gov.cn/quicklogin.htm',
			'data'=>'mobileNo={$mobile}&password=X{$time}&yzm=&isAjax=true',
			'post'=>true
		]
	],
	
	'ciming'=>[
		'parser'=>['string', 'register', ['str', 1]],
		'desc'=>'慈铭体检',
		
		'main'=> [
			'url'=>'http://book.ciming.com/userRegistPhoneCheck.html',
			'data'=>'loginPhone={$mobile}',
			'post'=>true,
		]
	],
	
	'yaofangwang'=>[
		'parser'=>['string', 'register', ['str', '1']],
		'desc'=>'药房网',
		
		'main'=> [
			'url'=>'http://reg.yaofangwang.com/Handler/Handler.ashx',
			'data'=>'timenow=1478674132&method=IsExistMobile&mobile={$mobile}',
		]
	],
	
	'dxy'=>[
		'parser'=>['json', 'register', ['success', false]],
		'desc'=>'丁香园',
		
		'main'=> [
			'url'=>'https://auth.dxy.cn/api/accounts/reg/validate/phone',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],
	
	'120ask'=>[
		'parser'=>['json', 'register', ['result', 'no']],
		'desc'=>'有问必答',
		
		'main'=> [
			'url'=>'http://a.120ask.com/unifyreg',
			'data'=>'phone={$mobile}&mark=checkphone',
		]
	],
	
	'kq88'=>[
		'parser'=>['json', 'register', ['status', 0]],
		'desc'=>'口腔医学网',
		
		'main'=> [
			'url'=>'http://hy.kq88.com/index.php?s=/Headpage/RegisterPhoneRrevise/isUsephone',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],
];