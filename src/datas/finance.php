<?php
/**
 * 金融，理财，保险，银行等
 */

return [
	'fengniaojr'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'蜂鸟金融',
		
		'main'=> [
			'url'=>'https://www.fengniaojr.com/validatephone.htm',
			'data'=>'phone={$mobile}',
			'post'=>true
		]
	],
	
	'pedaily'=>[
		'parser'=>['json', 'register', ['code', 0]],
		'desc'=>'投资界',
		
		'main'=> [
			'url'=>'http://newseed.pedaily.cn/check/mobile/',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'we'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'人人贷',
		
		'main'=> [
			'url'=>'http://www.we.com/checkEmail.action',
			'data'=>'username={$mobile}',
		]
	],
	
	'niwodai'=>[
		'parser'=>['string', 'register', ['regexp', "#请重新输入!\",\"state\"\:\"-2\"#"]],
		'desc'=>'你我贷',
		
		'main'=> [
			'url'=>'https://member.niwodai.com/index.do?method=doLoginAction1',
			'data'=>'codeType=0&geetestCode=geetest&username={$mobile}&pwd=X{$time}&geetest_challenge=&geetest_validate=&geetest_seccode=&channel=3&imgCode=&phoneCode=',
			'post'=>true,
		]
	],
	
	'lancai'=>[
		'parser'=>['json', 'register', ['data.registered', true]],
		'desc'=>'懒财网',
		
		'main'=> [
			'url'=>'https://api-main.lancai.cn/sys_check_mobile.php',
			'data'=>'{"mobile":{$mobile}}',
			'referer'=>'https://api-main.lancai.cn/cross_domain.html',
			'post'=>true,
		]
	],
	
	'jiedaibao'=>[
		'parser'=>['json', 'register', ['error.returnCode', 20211]],
		'desc'=>'借贷宝',
		
		'main'=> [
			'url'=>'http://tradeapi.jiedaibao.com/mybankv21/phppassport/v2/passport/account/wregister',
			'data'=>'phoneNumber={$mobile}&loginPassword=d0f80f71f4192c6f7e690820099d3a6x&udid=tgiycphslk{$time}993',
			'post'=>true
		]
	],
	
	'pingan'=>[
		'parser'=>['json', 'register', ['data.errorCode', '0']],
		'desc'=>'中国平安',
		
		'main'=> [
			'url'=>'https://member.pingan.com.cn/pinganone/pa/validateMobileNoForFcmm.view',
			'data'=>'checkMobileLogin=1&mobileNo={$mobile}&callback=jsonp1477972956175&_=1477972999103',
		]
	],
	
	'chinalife'=>[
		'parser'=>['string', 'register', ['str', 'phoneFailing']],
		'desc'=>'中国人寿',
		
		'main'=> [
			'url'=>'https://www.chinalife.com.cn/online/cn/com/chinalife/ebusiness/service/user/personal/checkCommunicat.do',
			'data'=>'emailOrMobilePhone={$mobile}',
			'post'=>true,
		]
	],
	
	'epicc'=>[
		'parser'=>['string', 'register', ['str', '1']],
		'desc'=>'中国人保',
		
		'main'=> [
			'url'=>'http://www.epicc.com.cn/ecenter/ecenterClub/loginRegisterNew/registerMobileAndEmail/checkRegisterMobile',
			'data'=>'registerMobile={$mobile}',
			'post'=>true
		]
	],
	
	'xiangrikui'=>[
		'parser'=>['json', 'register', ['code', 21020602]],
		'desc'=>'向日葵保险',
		
		'main'=> [
			'url'=>'http://passport.xiangrikui.com/account/register/valid_phone/{$mobile}',
			'data'=>'',
			'post'=>true
		]
	],
	
	'axatp'=>[
		'parser'=>['json', 'register', ['data', '手机号已存在']],
		'desc'=>'安盛天平',
		
		'main'=> [
			'url'=>'http://chexian.axatp.com/regCheck.do',
			'data'=>'type=memberMobile&val={$mobile}&timestamp=1480476287380',
		]
	],
	
	'e-nci'=>[
		'parser'=>['json', 'register', ['isPhoneExist', false]],
		'desc'=>'新华保险',
		
		'main'=> [
			'url'=>'https://passport.e-nci.com/register/phone.json',
			'data'=>'phone={$mobile}',
			'post'=>true
		]
	],
	
	'ab95569'=>[
		'parser'=>['string', 'register', ['regexp', "#用户登录密码错误#"]],
		'desc'=>'安邦保险',
		
		'main'=> [
			'url'=>'http://www.ab95569.com/user/BFTlogin.htm',
			'data'=>'userBase.userPwd=X{$time}&userBase.userName={$mobile}',
		]
	],
	
	'cignacmb'=>[
		'parser'=>['string', 'register', ['regexp', "#已经被注册#"]],
		'desc'=>'招商信诺',
		
		'main'=> [
			'url'=>'https://member.cignacmb.com/pc-member/checkUser.xhtml',
			'data'=>'mobile={$mobile}',
			'post'=>true,
			'mobile'=>false
		]
	],
	
	'cnfol'=>[
		'parser'=>['json', 'register', ['flag', "10002"]],
		'desc'=>'中金在线',
		
		'main'=> [
			'url'=>'https://passport.cnfol.com/userregister/ajaxcheckmobile',
			'data'=>'mobile={$mobile}&type=1',
			'post'=>true
		]
	],
	
	'iqianjin'=>[
		'parser'=>['str', 'register', ['regexp', "#被占用了#"]],
		'desc'=>'爱钱进',
		
		'main'=> [
			'url'=>'https://www.iqianjin.com/user/checkMobile',
			'data'=>'mobile={$mobile}&name={$mobile}',
			'post'=>true
		]
	],
	
	'lcbox'=>[
		'parser'=>['json', 'register', ['message', '错误的密码']],
		'desc'=>'理财盒子',
		
		'match'=>[
			'url'=>'http://m.lcbox.cn/login.html',
			'data'=>'_={$time}',
			'attributes'=>['csrf'=>"#name\=\"_csrf\"\s*value\=\"(.*?)\"#"]
		],
		
		'main'=> [
			'url'=>'http://api.lcbox.cn/login',
			'data'=>'account={$mobile}&password=X{$time}&_csrf={$csrf}',
			'post'=>true
		]
	],
	
	'okcoin'=>[
		'parser'=>['str', 'register', ['str', '0']],
		'desc'=>'OKCoin',
		
		'main'=> [
			'url'=>'https://www.okcoin.cn/user/reg/chcekregname.do',
			'data'=>'name={$mobile}&type=0&areaCode=86&random=81',
		]
	],
	
	'gaosouyi'=>[
		'parser'=>['json', 'register', ['info', "该手机号已经注册，请直接登录"]],
		'desc'=>'高搜易',
		
		'main'=> [
			'url'=>'http://hd.gaosouyi.com/theme/checkexistphone',
			'data'=>'param={$mobile}&name=phone',
			'post'=>true
		]
	],
	
	'eloancn'=>[
		'parser'=>['str', 'register', ['regexp', "#已存在#"]],
		'desc'=>'翼龙贷',
		
		'main'=> [
			'url'=>'http://user.eloancn.com/userbaseinfo/register/checkMobile',
			'data'=>'mobile={$mobile}',
			'post'=>true,
			'http_header'=>['Accept: application/json, text/javascript, */*; q=0.01']
		]
	],
	
	'p2peye'=>[
		'parser'=>['json', 'register', ['code', 7053]],
		'desc'=>'网贷天眼',
		
		'main'=> [
			'url'=>'http://www.p2peye.com/forum.php',
			'data'=>'mod=ajax&inajax=yes&infloat=register&handlekey=register&ajaxmenu=1&action=checkemobile_json&phonenum={$mobile}',
		]
	],

];