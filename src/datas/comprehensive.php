<?php
/**
 * 综合门户,咨询,论坛等
 */

return [
	'360cn'=>[
		'parser'=>['string', 'register', ['regexp', "#location\.href\=\'.*?errno\=30000\&#"]],
		'desc'=>'360',
	
		'main'=> [
			'url'=>'http://login.360.cn/',
			'data'=>'src=pcw_home&from=pcw_home&charset=UTF-8&requestScema=http&o=User&m=checkmobile&mobile={$mobile}&proxy=http://i.360.cn/psp_jump.html',
		]
	],
	
	'sina'=>[
		'parser'=>['json', 'register', ['retcode', 100001]],
		'desc'=>'新浪',
	
		'main'=> [
			'url'=>'https://login.sina.com.cn/signup/check_user.php',
			'referer'=>'https://login.sina.com.cn/signup/signup',
			'data'=>'name={$mobile}&format=json&from=mobile',
			'post'=>true,
		]
	],
	
	'qq'=>[
		'parser'=>['string', 'register', ['regexp', "#ptui_checkVC\(\'1\'#"]],
		'desc'=>'QQ/微信',
	
		'main'=> [
			'url'=>'http://check.ptlogin2.qq.com/check',
			'data'=>'regmaster=&pt_tea=2&pt_vcode=1&uin=@{$mobile}',
		]
	],
	
	'163com'=>[
		'parser'=>['json', 'register', ['code', 201]],
		'desc'=>'163',
		
		'list'=> [
			[
				'url'=>'http://reg.email.163.com/unireg/call.do',
				'data'=>'cmd=register.entrance&from=163navi&regPage=163'
			]
		],
	
		'main'=> [
			'url'=>'http://reg.email.163.com/unireg/call.do?cmd=added.mobilemail.checkBinding',
			'post'=>true,
			'data'=>'mobile={$mobile}',
		]
	],
	
	'cctv'=>[
		'parser'=>['json', 'register', ['msgHead.resCode', 'D200001']],
		'desc'=>'央视网',
		
		'main'=> [
			'url'=>'http://reg.cctv.com/regist/verifyMobile.do',
			'post'=>true,
			'data'=>'mobile={$mobile}',
		]
	],
	
	'58com'=>[
		'parser'=>['string', 'register', ['regexp', "#\"code\"\:772\,\"msg\"#"]],
		'desc'=>'58同城',
		
		'match'=> [
			'url'=> 'https://passport.58.com/login/',
			'data'=> 'PGTID=0d000000-0000-008e-cdd3-936adb66db6d&ClickID=1',
			'attributes'=> ['tokenId'=>"#id\=\"getTokenId_new\".*?value=\"(\w+)\"#"],
		],
		
		'main'=> [
			'url'=>'https://passport.58.com/login/dologin',
			'post'=>true,
			'data'=>'isweak=0&path=http://my.58.com/?pts=1477565848003&tokenId={$tokenId}&username={$mobile}&callback:successFun&fingerprint=_000&password=28dda48531fe44fa9ef8a499a0fdf1267bb690781b2400f0e0b0300499b142f1c403dbeccda8e16b72ab4dcdc91711029fe216a725291ba2e822aeb417e4bec7dfc027afd2505cf5fb17dbb2ed0ffb9cb06ef52e4c4924ade713be7e7c7ec4ee87e58a23c12805f389faaa36a3f63d00063e8a996770ee1d96f6757c0a413055',
		]
	],
	
	'ifeng'=>[
		'parser'=>['string', 'register', ['regexp', "#\"res\"\:\"(102|112|122)\"#"]],
		'desc'=>'凤凰网',
		
		'main'=> [
			'url'=>'https://id.ifeng.com/api/checkMobile?callback=cb',
			'data'=>'u={$mobile}',
			'post'=>true,
		]
	],
	
	'baidu'=>[
		'parser'=>['json', 'register', ['errInfo.no', 130020]],
		'desc'=>'百度',
	
		'list'=> [
			[
				'url'=> 'https://passport.baidu.com/v2/',
				'data'=> 'reg',
			]
		],
		
		'main'=> [
			'url'=>'https://passport.baidu.com/v2/',
			'data'=>'regphonecheck&apiver=v3&phone={$mobile}',
		]
	],
	
	'ganji'=>[
		'parser'=>['json', 'register', ['error', 1]],
		'desc'=>'赶集',
	
		'match'=> [
			'url'=> 'http://www.ganji.com/user/register.php',
			'data'=> 'callback=&buid=youku&template=tempA&regModel=mobile&size=normal',
			'attributes'=> ['__hash__'=>"#\,\"__hash__\"\:\"(.*?)\"\}#"],
		],
		
		'main'=> [
			'url'=>'http://www.ganji.com/user/ajax/checkPhone.php',
			'data'=>'reg_phone={$mobile}&__hash__={$__hash__}',
			'post'=>true,
		]
	],
	
	'sohu'=>[
		'parser'=>['json', 'register', ['code', 7]],
		'desc'=>'搜狐',

		'main'=> [
			'url'=>'https://passport.sohu.com/signup/check_mobile',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'zol'=>[
		'parser'=>['json', 'register', ['info', 'error']],
		'desc'=>'中关村在线',
		
		'match'=>[
			'url'=>'http://service.zol.com.cn/user/login.php',
			'data'=>'type=reg',
			'attributes'=> ['param'=>"#php\?param=(.*?)\'#"],
		],
		
		'list'=>[[
			'url'=>'https://service.zol.com.cn/captchasrc.php',
			'data'=>'param={$param}'
		]],

		'main'=> [
			'url'=>'http://service.zol.com.cn/user/ajax/login2014/checkRegUserid.php',
			'post'=>true,
			'data'=>'userid={$mobile}&act=check',
		]
	],
	
	'csdn'=>[
		'parser'=>['json', 'register', ['err', "2"]],
		'desc'=>'CSDN',
		
		'main'=> [
			'url'=>'http://passport.csdn.net/account/mobileregister',
			'data'=>'action=validateMobile&mobile={$mobile}',
		]
	],
	
	'tom'=>[
		'parser'=>['json', 'register', ['isUserMobileExist', false]],
		'desc'=>'Tom',
		
		'main'=> [
			'url'=>'http://web.mail.tom.com/webmail/register/veriMobile.action',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'hupu'=>[
		'parser'=>['json', 'register', ['code', 1039]],
		'desc'=>'虎扑体育',
		
		'main'=> [
			'url'=>'https://passport.hupu.com/pc/login/member.action',
			'data'=>'username={$mobile}&password=c790867e23e2950e082127b043e1878a',
			'post'=>true
		]
	],
	
	'19lou'=>[
		'parser'=>['json', 'register', ['success', false]],
		'desc'=>'19楼',
		
		'main'=> [
			'url'=>'http://www.19lou.com/register/checkmobile',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'hexun'=>[
		'parser'=>['json', 'register', ['msg', '密码输入错误']],
		'desc'=>'和讯',
		
		'list'=>[
			[
				'url'=>'http://www.hexun.com/',
				'data'=>'',
			],
			[
				'url'=>'http://reg.hexun.com/rest/loginbox.aspx',
				'data'=>'gourl=http%3A//www.hexun.com/'
			]
		],
		
		'main'=> [
			'url'=>'https://reg.hexun.com/rest/ajaxlogin.aspx',
			'data'=>'callback=jQuery1820749656105870625_1478599001694&username={$mobile}&password=Xz1228jdVv&gourl=http%253A%2F%2Ffunds.hexun.com%2F&fromhost=funds.hexun.com&hiddenReferrer=http%253A%2F%2Ffunds.hexun.com%2F&loginstate=1&code=%25u9A8C%25u8BC1%25u7801&act=login&_=1478599015055',
			'referer'=>'http://reg.hexun.com/rest/loginbox.aspx?gourl=http%3A//funds.hexun.com/',
		]
	],
	
	'people'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'人民网',
		
		'main'=> [
			'url'=>'http://sso.people.com.cn/u/reg/checkPhoneNum',
			'data'=>'phoneNum={$mobile}&_=1478527381760',
		]
	],
	
	'china'=>[
		'parser'=>['string', 'register', ['str', '_ck("mobileIsBind")']],
		'desc'=>'中华网',
		
		'main'=> [
			'url'=>'http://passport1.china.com/jdzzFrontValidate.do',
			'data'=>'username={$mobile}@mail.china.com,true',
		]
	],
	
	'huxiu'=>[
		'parser'=>['json', 'register', ['error.message', '密码不匹配']],
		'desc'=>'虎嗅',
		
		'main'=> [
			'url'=>'https://m.huxiu.com/user_action/login',
			'data'=>'username={$mobile}&country=%2B86&password=X{$time}',
			'post'=>true
		]
	],
	
	'rayli'=>[
		'parser'=>['string', 'register', ['regexp', "#\{\'mobile\'\:\'-3\'\}#"]],
		'desc'=>'瑞丽',
		
		'main'=> [
			'url'=>'http://user.rayli.com.cn/forum.php',
			'data'=>'mod=ajax&infloat=register&handlekey=register&action=checkmobile&ajaxmenu=1&mobile={$mobile}&inajax=1&ajaxtarget=passport_tip',
		]
	],
	
	'cnmo'=>[
		'parser'=>['json', 'register', ['status', 3]],
		'desc'=>'手机中国',
		
		'main'=> [
			'url'=>'http://passport.cnmo.com/index.php?c=Member_Ajax_Register&m=CheckMobileInuse',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'fengniao'=>[
		'parser'=>['json', 'register', ['code', '-7']],
		'desc'=>'蜂鸟网',
		
		'list'=>[
			['url'=>'http://my.fengniao.com/user/login', 'data'=>''],
		],
		
		'main'=> [
			'url'=>'http://my.fengniao.com/ajax/my/loginAjax.php?callback=cb',
			'data'=>'do=login&name={$mobile}&password=d112d29cdd7d561a8b33a419a35e3bex&code=',
			'ip'=>false,
			'post'=>true,
		]
	],
	
	'yaolan'=>[
		'parser'=>['json', 'register', ['status', 3]],
		'desc'=>'摇篮网',
		
		'main'=> [
			'url'=>'http://user.yaolan.com/AjaxValidate.aspx',
			'data'=>'type=5&mobilePhone={$mobile}',
			'post'=>true
		]
	],
];