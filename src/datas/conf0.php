<?php
return [
	'zhcw'=>[
		'parser'=>['json', 'register', ['resCode', '0005']],
		'desc'=>'中彩网',
		
		'main'=> [
			'url'=>'http://tzds.zhcw.com/home/do.php?ac=register&op=checkmobile',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'500com'=>[
		'parser'=>['json', 'register', ['code', -6]],
		'desc'=>'500彩票',
		
		'list'=>[
			[
				'url'=>'https://passport.500.com/user/',
				'data'=>'',
			],
			[
				'url'=>'https://cs.500.com/dcs.gif',
				'data'=>'dcsdat=1478085174996&dcssip=passport.500.com&dcsuri=/user/&sdc_userflag=1478085174998::1478085174998::1&sdc_session=1478085174998&WT.load_t=2756&WT.tz=8&WT.bh=19&WT.ul=zh-CN&WT.cd=24&WT.sr=2560x1440&WT.bs=945x982&WT.browser=945x965&WT.fv=21.0&WT.vt_f_s=1&WT.vt_f_d=1&WT.co_f=undefined&WT.vt_sid=undefined.1478085174997'
			]
		],
		
		'main'=> [
			'url'=>'https://passport.500.com/user/ajax_reg.php',
			'referer'=>'https://passport.500.com/user/',
			'data'=>'type=1&back=_checkUsername&inputdata={$mobile}&_0.96260344183702=',
		]
	],
	
	'huoban'=>[
		'parser'=>['json', 'register', ['message', '密码错误']],
		'desc'=>'伙伴云表格',
		
		'main'=> [
			'url'=>'https://token.huoban.com/token',
			'data'=>'{"username":"{$mobile}","password":"zX{$time}","client_id":1,"grant_type":"password"}',
			'http_header'=>['Content-Type:application/json', 'Origin:https://app.huoban.com'],
			'referer'=>'https://app.huoban.com/account/login',
			'post'=>true,
		]
	],
	
	'huaban'=>[
		'parser'=>['json', 'register', ['msg', '用户密码错误']],
		'desc'=>'花瓣网',
		
		'main'=> [
			'url'=>'https://huaban.com/auth/',
			'data'=>'email={$mobile}&password=ABx{$time}&_ref=frame',
			'post'=>true,
		]
	],
	
	'yos'=>[
		'parser'=>['json', 'register', ['msg', '用户名或密码错误']],
		'desc'=>'吉野家',
		
		'match'=>[
			'url'=>'http://www.4008-197-197.com/',
			'data'=>'',
			'attributes'=>['key'=>"#key\s*\=\s*\"(.*?)\"\;#"]
		],
		
		'main'=> [
			'url'=>'http://www.4008-197-197.com/Action?serviceId=4&actionId=11',
			'data'=>'{"loginKey":"{$mobile}","passwd":"585a8d51901f48195c86e5c2809b403f","key":"{$key}"}',
			'http_header'=>['Content-Type: multipart/form-data'],
			'post'=>true
		]
	],
	
	'haidilao'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'海底捞',
		
		'main'=> [
			'url'=>'http://hi.haidilao.com/ajax/logins/searchInByMobile.action',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'xunlei'=>[
		'parser'=>['json', 'register', ['binded', 1]],
		'desc'=>'迅雷',
		
		'list'=>[
			[
				'url'=>'https://login.xunlei.com/risk?cmd=report',
				'data'=>'xl_fp_raw='.urlencode('P#etCxXneIMfMG3q###Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36###zh-CN###24###1440x2560###-480###true###true###true###undefined###function######Win32######Widevine Content Decryption Module::Enables Widevine licenses for playback of HTML audio/video content. (version: 1.4.8.823)::application/x-ppapi-widevine-cdm~;Chrome PDF Viewer::::application/pdf~pdf;Shockwave Flash::Shockwave Flash 21.0 r0::application/x-shockwave-flash~swf,application/futuresplash~spl;Native Client::::application/x-nacl~,application/x-pnacl~;Chrome PDF Viewer::Portable Document Format::application/x-google-chrome-pdf~pdf###22b4692f98f0d7a48ee37728981cd1c5###7u$v$2ZFjPZYMt3e').'&xl_fp=ab4cb6564707076752feb2606c2b4423&cachetime=1479793232101',
				'post'=>true
			]
		],
		
		'main'=> [
			'url'=>'https://zhuce.xunlei.com/regapi',
			'data'=>'callback=jsonp1479284837287&op=checkBind&account={$mobile}&type=1&response=jsonp',
			'referer'=>'http://i.xunlei.com/register.html?regfrom=KF_BBS51',
			'mobile'=>false
		]
	],
	
	'bjjzzpt'=>[
		'parser'=>['json', 'register', ['status', 'n']],
		'desc'=>'北京居住证',
		
		'main'=> [
			'url'=>'https://www.bjjzzpt.com/bjsjzzfwpt/userQt/isUsed',
			'data'=>'param={$mobile}&name=user_sjh',
			'post'=>true
		]
	],
	
	'xinnet'=>[
		'parser'=>['string', 'register', ['str', 'isExist']],
		'desc'=>'新网',
		
		'main'=> [
			'url'=>'http://www.xinnet.com/userRegister/userRegister.do?method=ifMobilePhoneExist',
			'data'=>'userMobiNumber={$mobile}',
			'post'=>true
		]
	],
];
