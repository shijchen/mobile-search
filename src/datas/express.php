<?php
/**
 * 快递，物流，运输等相关
 */
 
return [
	'sfexpress'=>[
		'parser'=>['string', 'register', ['str', '"Y"']],
		'desc'=>'顺丰',
		
		'main'=> [
			'url'=>'https://i.sf-express.com/service/new/user/register/common/mobile/same',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],
	
	'sto'=>[
		'parser'=>['str', 'register', ['str', '2']],
		'desc'=>'申通',
		
		'main'=> [
			'url'=>'http://q1.sto.cn/reg/accountexist',
			'data'=>'account={$mobile}',
		]
	],
	
	'360hitao'=>[
		'parser'=>['str', 'register', ['regexp', "#repeat#"]],
		'desc'=>'百世集团',
		
		'main'=> [
			'url'=>'http://member.360hitao.com/reg/reg.ashx',
			'data'=>'methods=mobile&mobile={$mobile}&_=1481174553232',
		]
	],
	
	'zto'=>[
		'parser'=>['str', 'register', ['str', 'false']],
		'desc'=>'中通',
		
		'main'=> [
			'url'=>'http://my.zto.com/Account/CheckPhoneIsNotExisted',
			'data'=>'MobielPhone={$mobile}',
		]
	],
	
	'yundaex'=>[
		'parser'=>['json', 'register', ['result', false]],
		'desc'=>'韵达',
		
		'main'=> [
			'url'=>'http://member.yundaex.com/ydmb/service/accountno/check_login_name.json',
			'data'=>'loginName={$mobile}',
			'post'=>true
		]
	],
	
	'rufengda'=>[
		'parser'=>['str', 'register', ['str', 'false']],
		'desc'=>'如风达',
		
		'main'=> [
			'url'=>'http://www.rufengda.com/front/findCustomerByMobileForVali.do',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'transrush'=>[
		'parser'=>['str', 'register', ['str', 'false']],
		'desc'=>'运转四方',
		
		'main'=> [
			'url'=>'http://passport.transrush.com/AjaxIndex.aspx',
			'data'=>'type=11&Phone={$mobile}',
		]
	],
	
	'kuaidihelp'=>[
		'parser'=>['string', 'register', ['str', 2]],
		'desc'=>'快递助手',
		
		'main'=> [
			'url'=>'http://www.kuaidihelp.com/reg/accountexist',
			'data'=>'account={$mobile}',
		]
	],
	
	'xiaoniu56'=>[
		'parser'=>['str', 'register', ['regexp', "#密码错误#"]],
		'desc'=>'小牛物流',
		
		'main'=> [
			'url'=>'http://www.xiaoniu56.com/user/login',
			'data'=>'mobile={$mobile}&password=X{$time}',
			'post'=>true
		]
	],
];