<?php
/**
 * 汽车,房产相关
 */

return [
	'fang'=>[
		'parser'=>['json', 'register', ['IsBingding', true]],
		'desc'=>'房天下',
		
		'main'=> [
			'url'=>'https://passport.fang.com/checkPhonebinding.api',
			'post'=>true,
			'data'=>'MobilePhone={$mobile}&Service=soufun-passport-web',
		]
	],
	
	'yiche'=>[
		'parser'=>['json', 'register', ['mobile.status', false]],
		'desc'=>'易车网',
		
		'main'=> [
			'url'=>'http://i.yiche.com/ajax/authenservice/MobileCode.ashx',
			'post'=>true,
			'data'=>'mobileCodeKey=register&smsType=0&mobile={$mobile}',
		]
	],
	
	'xcar'=>[
		'parser'=>['json', 'register', ['error', 4]],
		'desc'=>'爱卡汽车',
		
		'match'=>[
			'url'=>'http://my.xcar.com.cn/logging.php',
			'data'=>'',
			'attributes'=> ['unique'=>"#id\=\"unique\"\s*value\=\"(.*?)\"#"],
		],
		
		'main'=> [
			'url'=>'http://my.xcar.com.cn/ajax/ajax_dologin.php',
			'post'=>true,
			'data'=>'username={$mobile}&userpwd=weweweasa&logintype=2&checkcode=&uniquekey={$unique}',
		]
	],
	
	'autohome'=>[
		'parser'=>['json', 'register', ['success', 0]],
		'desc'=>'汽车之家',
		
		'main'=> [
			'url'=>'http://account.autohome.com.cn/AccountApi/CheckPhone',
			'data'=>'isOverSea=0&validcodetype=1&phone={$mobile}',
			'post'=>true,
		]
	],
	
	'pcauto'=>[
		'parser'=>['json', 'register', ['status', 43]],
		'desc'=>'太平洋汽车',
		
		'main'=> [
			'url'=>'http://passport3.pcauto.com.cn/passport3/api/validate_mobile.jsp?mobile={$mobile}&req_enc=UTF-8',
			'data'=>'{}',
			'post'=>true,
		]
	],
	
	'chinacar'=>[
		'parser'=>['str', 'register', ['str', 'ReRegis']],
		'desc'=>'中国汽车网',
		
		'main'=> [
			'url'=>'http://www.chinacar.com.cn/Home/Login/Login/checkTel',
			'data'=>'telphone={$mobile}&r=0.2599756917'.rand(10001, 90009),
		]
	],
	
	'anjuke'=>[
		'parser'=>['string', 'register', ['str', '"userUsed"']],
		'desc'=>'安居客',
		
		'main'=> [
			'url'=>'http://vip.anjuke.com/broker/register/',
			'data'=>'mobile={$mobile}&tmp=1478077517292',
		]
	],
	
	'cheshi'=>[
		'parser'=>['string', 'register', ['str', '-3']],
		'desc'=>'网上车市',
		
		'list'=>[[
			'url'=>'http://service.cheshi.com/user/register.php',
			'data'=>''
		]],
		
		'main'=> [
			'url'=>'http://service.cheshi.com/user/ajax/reg_ajax.php',
			'referer'=>'http://service.cheshi.com/user/register.php',
			'http_header'=>['Cookie:pv_uid=1478145780174'],
			'data'=>'act=checkMobile&mobile={$mobile}',
		]
	],
	
	'jia'=>[
		'parser'=>['string', 'register', ['regexp', "#用户名或密码不正确#"]],
		'desc'=>'齐家',
		
		'main'=> [
			'url'=>'https://passport.jia.com/cas/login?app_id=301&skip=true&service=http%3A%2F%2Fwww.jia.com%2Fbeijing%2F',
			'data'=>'loginName={$mobile}&password=X{$time}&rememberName=on',
			'post'=>true,
		]
	],
	
	'yihaojiaju'=>[
		'parser'=>['json', 'register', ['Code', 1303]],
		'desc'=>'一号家居',
		
		'match'=>[
			'url'=>'https://passport.yihaojiaju.com/login.html',
			'data'=>'',
			'attributes'=>['sid'=>"#id\=\"HidSessionID\"\s*value\=\"(.*?)\"#"]
		],
		
		'main'=> [
			'url'=>'https://passport.yihaojiaju.com/Hander/Login.ashx?action=NewLogin',
			'data'=>'LoginId={$mobile}&PassWord=X{$time}&key=logincode{$sid}&code=',
			'post'=>true,
			'ip'=>false
		]
	],
	
	'tobosu'=>[
		'parser'=>['json', 'register', ['error_code', 250]],
		'desc'=>'土拨鼠',
		
		'main'=> [
			'url'=>'http://www.tobosu.com/tapi/passport/query_mobile',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'5i5j'=>[
		'parser'=>['json', 'register', ['status', -3]],
		'desc'=>'我爱我家',
		
		'main'=> [
			'url'=>'http://bj.5i5j.com/regLogin/ajaxLogin',
			'data'=>'username={$mobile}&password=X{$time}&type=telphone',
			'post'=>true,
			'mobile'=>false,
		]
	],
	
	'ziroom'=>[
		'parser'=>['json', 'register', ['resp.exist', true]],
		'desc'=>'自如',
		
		'main'=> [
			'url'=>'http://passport.ziroom.com/api/index.php?r=user/verify-account',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],
	
];