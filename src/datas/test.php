<?php
/**
 * 测试
 */

if(!defined('LOCAL') || !LOCAL) return [];

return [
	'top1'=>[
		'parser'=>['json', 'register', ['error', '0', false]],
		'desc'=>'至尊租车',
		
		'main'=> [
			'url'=>'https://www.top1.cn/home/Reg.aspx',
			'data'=>'act=check_user_reg_small&clientid=MobilePhoneNo&MobilePhoneNo={$mobile}&_=1478152193441',
		]
	],
	
	'etongdai'=>[
		'parser'=>['json', 'register', ['code', null, false]],
		'desc'=>'易通贷',
		
		'main'=> [
			'url'=>'http://app.etongdai.com/register/checkPhones',
			'data'=>'phones={$mobile}',
			'post'=>true,
		]
	],
	
	'ssrj'=>[
		'parser'=>['string', 'register', ['str', "false"]],
		'desc'=>'时尚日记',
		
		'main'=> [
			'url'=>'http://www.ssrj.com/register/check_mobile.jhtml',
			'data'=>'mobile={$mobile}&_=1478241541770',
		]
	],
	
	'yohobuy'=>[
		'parser'=>['json', 'register', ['code', "200", false]],
		'desc'=>'有货',
		
		'main'=> [
			'url'=>'http://www.yohobuy.com/passport/reg/checkmobile',
			'data'=>'mobile={$mobile}&area=86',
			'mobile'=>false,
			'post'=>true,
		]
	],
	
	'7wenta'=>[
		'parser'=>['json', 'register', ['resultCode.code', -1]],
		'desc'=>'问他',
		
		'main'=> [
			'url'=>'http://account.7wenta.com/checkMobileExist.action',
			'data'=>'mobileNo={$mobile}',
			'post'=>true,
		]
	],
	
	'gamersky'=>[
		'parser'=>['json', 'register', ['StatusCode', 1, false]],
		'desc'=>'游民星空',
		
		'main'=> [
			'url'=>'http://i.gamersky.com/user/verifyphone',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],

	'bilibili'=>[
		'parser'=>['json', 'register', ['status', false]],
		'desc'=>'bilibili',
		
		'main'=> [
			'url'=>'https://passport.bilibili.com/register/checkTelFormat',
			'data'=>'tel={$mobile}&country_code=1',
			'post'=>true,
		]
	],

	'liepin'=>[
		'parser'=>['json', 'register', ['code', "200993017", false]],
		'desc'=>'猎聘',
		
		'main'=> [
			'url'=>'https://passport.liepin.com/c/login.json?__mn__=user_login',
			'data'=>'user_kind=0&isMd5=1&layer_from=wwwindex_top_cover_login_userc&user_pwd=271172738786944d3c2feca491720ccx&user_login={$mobile}&chk_remember_pwd=on',
			'post'=>true
		]
	],
	
	'news'=>[
		'parser'=>['json', 'register', ['desc', "", false]],
		'desc'=>'新华网',
		
		'main'=> [
			'url'=>'http://login.home.news.cn/profile/verifyMobilePhone.do',
			'data'=>'name=phone&value={$mobile}&type=json&t=1478574129840',
		]
	],
	
	'dobest'=>[
		'parser'=>['json', 'register', ['return_code', 0, false]],
		'desc'=>'游卡',
		
		'main'=> [
			'url'=>'http://cas.dobest.com/authen/checkAccountType.jsonp',
			'data'=>'callback=cb&serviceUrl=register.dobest.com&appId=205&areaId=0&authenSource=2&inputUserId={$mobile}&locale=zh_CN&productId=1&productVersion=1.7&version=21&_=1478581215329',
		]
	],
	
	'zeze'=>[
		'parser'=>['json', 'register', ['message', "successful", false]],
		'desc'=>'啧啧',
		
		'main'=> [
			'url'=>'http://www.zeze.com/forum.php',
			'data'=>'mod=ajax&action=checkphone&phone={$mobile}&_=1478674672547',
		]
	],
	
	'askci'=>[
		'parser'=>['json', 'register', ['ReturnMsg', "手机号码可用", false]],
		'desc'=>'千数堂',
		
		'main'=> [
			'url'=>'http://d.askci.com/Customer/CheckHaveAlreadyUsed/',
			'data'=>'para=手机号码&paraValue={$mobile}',
			'post'=>true
		]
	],
	
	'99com'=>[
		'parser'=>['json', 'register', ['msg', '', false]],
		'desc'=>'99健康网',
		
		'main'=> [
			'url'=>'http://my.99.com.cn/index.php?m=ajax&a=checkuser',
			'data'=>'in_ajax=1&value={$mobile}&callback=jsonp1478678554465',
			'post'=>true,
		]
	],
	
	'9ku'=>[
		'parser'=>['json', 'register', ['msg', '用户不存在，请重新注册', false]],
		'desc'=>'九酷云音乐',
		
		'main'=> [
			'url'=>'http://yun.9ku.com/user/api/login',
			'data'=>'input={"username":"{$mobile}","password":"X{$time}"}',
			'post'=>true,
		]
	],
	
	'mier123'=>[
		'parser'=>['xml', 'register', ['0', "succeed", false]],
		'desc'=>'米尔网',
		
		'list'=>[['url'=>'http://bbs.mier123.com/member.php', 'data'=>'mod=register']],
		
		'main'=> [
			'url'=>'http://bbs.mier123.com/forum.php',
			'data'=>'mod=ajax&inajax=yes&infloat=register&handlekey=register&ajaxmenu=1&action=checkMobile&mobile={$mobile}',
			'referer'=>'http://bbs.mier123.com/member.php?mod=register',
			'mobile'=>false
		]
	],
	
	'aipai'=>[
		'parser'=>['json', 'register', ['bid', '', false]],
		'desc'=>'爱拍',
		
		'main'=> [
			'url'=>'http://www.aipai.com/bus/urs/usercheck.php',
			'data'=>'callback=cb&&callback=checkPhoneSuc&metadata=%7B%22email%22:%22{$mobile}%22%7D',
		]
	],
	
	'bianfeng'=>[
		'parser'=>['json', 'register', ['existing', true]],
		'desc'=>'边锋',
		
		'main'=> [
			'url'=>'http://authleqr.bianfeng.com/lars/check-account-types.jsonp',
			'data'=>'callback=cb&userId={$mobile}&_=1478852864673',
		]
	],
	
	'lanfw'=>[
		'parser'=>['string', 'register', ['str', "false"]],
		'desc'=>'蓝房网',
		
		'list'=>[['url'=>'http://user.lanfw.com/index.php', 'data'=>'m=register&a=index&restype=gr&utype=0']],
		
		'main'=> [
			'url'=>'http://user.lanfw.com/index.php',
			'data'=>'m=register&a=ifmobile&clientid=shouji&rand=1479181704844&username=&mobile={$mobile}&verify=&_=1479181701062',
			'mobile'=>false
		]
	],
	
	'winxuan'=>[
		'parser'=>['json', 'register', ['result', true]],
		'desc'=>'文轩',
		
		'main'=> [
			'url'=>'https://passport.winxuan.com/signup/mobile/check',
			'data'=>'mobile={$mobile}',
		]
	],
	
	'xiaolajiao'=>[
		'parser'=>['string', 'register', ['str', 'ok', false]],
		'desc'=>'小辣椒',
		
		'main'=> [
			'url'=>'http://account.xiaolajiao.com/eventlib/checkevent.php',
			'data'=>'act=mobilephone&mobile={$mobile}&time=Tue%20Nov%2015%202016%2016:59:47%20GMT+0800%20(%E4%B8%AD%E5%9B%BD%E6%A0%87%E5%87%86%E6%97%B6%E9%97%B4)',
		]
	],
	
	'doov'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'朵唯',
		
		'main'=> [
			'url'=>'http://www.doov.com.cn/register/check_username.jhtml',
			'data'=>'username={$mobile}&_=1479200504882',
		]
	],
	
	'ktouch'=>[
		'parser'=>['json', 'register', ['RegisterId', 0, false]],
		'desc'=>'天语',
		
		'main'=> [
			'url'=>'http://www.k-touch.cn/Home/CheckEmail',
			'data'=>'Parm={$mobile}&Type=2',
			'post'=>true
		]
	],
	
	'ppdai'=>[
		'parser'=>['json', 'register', ['Code', '1', false]],
		'desc'=>'拍拍贷',
		
		'main'=> [
			'url'=>'https://ac.ppdai.com/registercheck',
			'data'=>'callback=cb&name=mobilePhone&value={$mobile}&_=1480902360026',
		]
	],
	
	'lu'=>[
		'parser'=>['json', 'register', ['resultId', '01', false]],
		'desc'=>'陆金所',
		
		'main'=> [
			'url'=>'https://user.lu.com/user/service/user/check-phone-is-used',
			'data'=>'newPhoneNum={$mobile}&enterpriseFlag=0&_=1480903840735',
		]
	],
	
	'edai'=>[
		'parser'=>['string', 'register', ['str', '1']],
		'desc'=>'易贷网',
		
		'main'=> [
			'url'=>'http://www.edai.com/user/exist/',
			'data'=>'phone={$mobile}',
			'post'=>true
		]
	],
	
	'west'=>[
		'parser'=>['str', 'register', ['str', '', false]],
		'desc'=>'西部数码',
		
		'main'=> [
			'url'=>'http://www.west.cn/reg/default.asp',
			'data'=>'act=check&inputname=msn&inputvalue={$mobile}',
			'post'=>true
		]
	],
	
	'tuandai'=>[
		'parser'=>['json', 'register', ['result', 'True']],
		'desc'=>'团贷网',
		
		'main'=> [
			'url'=>'https://www.tuandai.com/user/new_tuandai_ajax/Reg.ashx',
			'data'=>'Cmd=CheckPhone&sPhone={$mobile}',
			'referer'=>'https://www.tuandai.com/user/register.aspx',
			'post'=>true,
		]
	],
	
	'huanqiu'=>[
		'parser'=>['json', 'register', ['msg', '&nbsp;', false]],
		'desc'=>'环球网',
		
		'main'=> [
			'url'=>'https://i.huanqiu.com/index.php?g=auth&m=members&a=check_mobile',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'btime'=>[
		'parser'=>['json', 'register', ['errno', 2000, false]],
		'desc'=>'北京时间',
		
		'main'=> [
			'url'=>'http://user.btime.com/loging?callback=jQuery11020042286674945868574_1481018253657',
			'data'=>'user_name={$mobile}&passwd=VeiTVGr6lIg%2By7TXt5ZNIWWyn%2BpG8REPQezH6t9ZbUJf%2BYOxFgNYqCJWXHCL6BGpZhWZ4cPJ97mXtmMHvwLAwX1zx01HoE7QFmhU0rz5eSaXkJ2cCn3yM%2BiHKnBp9UqbXTRtQalmw0WQaDbgqdHKdJp1NCIxJOqlM%2FqQyGTHCBQ%3D&remember=on&timestamp=1481018253812&jump=http%253A%252F%252Fuser.btime.com%252FviewShow%253Fgate%253Dregister',
		]
	],
	
	'chinabuses'=>[
		'parser'=>['str', 'register', ['str', '1', false]],
		'desc'=>'中国客车网',
		
		'main'=> [
			'url'=>'http://www.chinabuses.com/index.php',
			'data'=>'clientid=mobile&mobile={$mobile}&m=member&c=index&a=public_checkmobile_ajax&_=1481101603722',
		]
	],
	
	'12308com'=>[
		'parser'=>['str', 'register', ['str', '"1"', false]],
		'desc'=>'12308',
		
		'main'=> [
			'url'=>'http://uc.12308.com/register/checkMobilePhone.sc?mobilePhone={$mobile}',
			'data'=>'mobilePhone={$mobile}',
			'post'=>true
		]
	],
	
	'baobeio'=>[
		'parser'=>['str', 'register', ['str', '1', false]],
		'desc'=>'宝贝网',
		
		'main'=> [
			'url'=>'http://www.baobeio.com/ftry/ajaxCheckMobile',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'zhaopi'=>[
		'parser'=>['str', 'register', ['regexp', "#账号不存在#", false]],
		'desc'=>'找皮网',
		
		'main'=> [
			'url'=>'http://www.zhaopi.cc/Login',
			'data'=>'ReturnUrl=&Username={$mobile}&Password=X{$time}&RememberMe=false',
			'post'=>true
		]
	],
	
	'mammytuan'=>[
		'parser'=>['json', 'register', ['data', 0, false]],
		'desc'=>'妈咪团',
		
		'main'=> [
			'url'=>'http://www.mammytuan.com/ajax/validator.php?n=signupmobile&v={$mobile}',
			'data'=>'',
			'post'=>true
		]
	],
	
	'lamatalk'=>[
		'parser'=>['json', 'register', ['mobile_registered', true]],
		'desc'=>'辣妈说',
		
		'main'=> [
			'url'=>'http://www.lamatalk.com/user/check_mobile',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'yeesha'=>[
		'parser'=>['json', 'register', ['status', 0, false]],
		'desc'=>'一生一纱',
		
		'main'=> [
			'url'=>'http://www.yeesha.com/index.php?route=newsite/personal/register/getPhone',
			'data'=>'phone={$mobile}',
			'post'=>true
		]
	],
	
	'yoka'=>[
		'parser'=>['str', 'register', ['str', '1', false]],
		'desc'=>'YOKA',
		
		'main'=> [
			'url'=>'http://passport.yoka.com/checkmobile.php',
			'data'=>'mobile={$mobile}&action=checkmobile',
			'post'=>true
		]
	],
	
	'deppon'=>[
		'parser'=>['str', 'register', ['regexp', "#已被注册#"]],
		'desc'=>'德邦',
		
		'main'=> [
			'url'=>'https://www.deppon.com/user/isMobileOk.action',
			'data'=>'user.telephone={$mobile}',
			'post'=>true
		]
	],
	
	'hfax'=>[
		'parser'=>['json', 'register', ['retMsg', "0000", false]],
		'desc'=>'惠金所',
		
		'main'=> [
			'url'=>'https://www.hfax.com/isrepeat.do',
			'data'=>'paramMap.userName={$mobile}',
			'post'=>true
		]
	],
	
	'btctrade'=>[
		'parser'=>['json', 'register', ['msg', '', false]],
		'desc'=>'比特币交易网',
		
		'main'=> [
			'url'=>'https://m.btctrade.com/ajax/user/email/15801190077/actype/2',
			'data'=>'',
		]
	],
	
	'huobi'=>[
		'parser'=>['json', 'register', ['msg', '', false]],
		'desc'=>'火币网',
		
		'main'=> [
			'url'=>'https://www.huobi.com/account/register.php',
			'data'=>'a=check_username&email_or_phone={$mobile}&type=phone&r=0.8261152959423317',
		]
	],
	
	'btc123'=>[
		'parser'=>['json', 'register', ['isSuc', false]],
		'desc'=>'BTC123',
		
		'main'=> [
			'url'=>'https://www.btc123.com/user/check/checkUser',
			'data'=>'user={$mobile}&type=2',
		]
	],
	
	'yyfax'=>[
		'parser'=>['str', 'register', ['str', 'true']],
		'desc'=>'友金所',
		
		'main'=> [
			'url'=>'https://www.yyfax.com/checkNameExists.htm',
			'data'=>'accountName={$mobile}',
			'post'=>true
		]
	],
	
	'p2p001'=>[
		'parser'=>['json', 'register', ['content', 'yes', false]],
		'desc'=>'第一网贷',
		
		'main'=> [
			'url'=>'http://www.p2p001.com/common/ckephone/',
			'data'=>'txt_phone={$mobile}',
			'post'=>true
		]
	],
	
	'anxin'=>[
		'parser'=>['str', 'register', ['str', 'False']],
		'desc'=>'安心贷',
		
		'main'=> [
			'url'=>'http://www.anxin.com/ajax/ajax.ashx',
			'data'=>'sMobile={$mobile}&cmd=CheckUserMobile',
			'post'=>true
		]
	],
	
	'itouzi'=>[
		'parser'=>['json', 'register', ['info', '', false]],
		'desc'=>'爱投资',
		
		'main'=> [
			'url'=>'https://www.itouzi.com/apiService/phone/isCertified',
			'data'=>'phone={$mobile}',
		]
	],
	
	/*
	'suning'=>[
		'parser'=>['json', 'register', ['errorCode', 'badPassword.msg1']],
		'desc'=>'苏宁易购',
		
		'list'=>[
			[
				'url'=>'https://passport.suning.com/ids/login',
				'data'=>'',
			],
			[
				'url'=>'https://fp.suning.com/bennu-collector/fp/porto.json',
				'data'=>'p0=f%5E%5Ef%5E%5EHZLZkQIxDESj2X_rsGQHsIEABWEodl5TxYxtWX1peB6_95Gvh3nGPtn9-tStfGZ8qt7vv_3PL-pMlE9ETvSe2Owrxm18jVOoiRUTR_c1e8_dZ3aP3nfKptc0b59u4Gqt6TtnTZiBp4nGXBBRDkPAEDX20ISBENISsjVXbbT0mXvREsseW8jvHPuh6dZ5UQ_OUsJsHNziO5yaywcrLsPpVRK8R6vPxohshZTr6THk7MejFayjXeCCPSMw4pv4tzzpDP6o9_KAP7KNdgcp4L9ga6HBHSM2pmBoF_5LiYO71ipMwiktH9fQ8epMw8kdQdYjTvE1K7Wrz0WuYIIJPpmaPltyl_QlXuS94ClhOWeP469Mq2pnXHmZgR9xwEeuMJ2FpRbgjx7VwJE3mWGSI5lfgs3Snq9Lf6KXeEzyJrpJpsRHbu7wl_oXoJFbPPTt-AI%3D%5E%5E164%5E%5Et%5E%5Et%5E%5Et%5E%5Et%5E%5Et%5E%5Et%5E%5Et%5E%5Ef%5E%5Ef%5E%5Et%5E%5Et%5E%5Et%5E%5Et%5E%5Ef%5E%5Ef%5E%5Ezh-CN%5E%5Ef%5E%5EGoogle%20Pepper%5E%5E-%5E%5EWindows%207%5E%5E1%5E%5EPlugIn%5E%5Ecolor%5E%5E72%5E%5E2560%5E%5E1440%5E%5Ee09487e6a9234c99854888b0ba77609c%5E%5EWIN%2021%2C0%2C0%2C242&p1=Mozilla%5E%5E-%5E%5ENetscape%5E%5E5.0%20(Windows%20NT%206.1%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F51.0.2704.79%20Safari%2F537.36%5E%5E-%5E%5Et%5E%5E-%5E%5E-%5E%5EWin32%5E%5E-%5E%5EGecko%5E%5E20030107%5E%5E-%5E%5EMozilla%2F5.0%20(Windows%20NT%206.1%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F51.0.2704.79%20Safari%2F537.36%5E%5E-%5E%5EGoogle%20Inc.%5E%5E-&p2=1400%5E%5E0%5E%5E0%5E%5E2560%5E%5E24%5E%5E1440%5E%5E24%5E%5E2560&p3=passport%5E%5E00df0013008d003b7264%5E%5EMTQs3OD3U4NaTQznOTIg5Mww%3D%3D0%5E%5E6efc8e76-dd46-45a0-9915-347f5bc3f28b%5E%5E81b0b6e0f45f0cbaeb009f468efd21a1%5E%5E13%3D%3Dgo%5E%5Enone%5E%5E123%5E%5E123%5E%5E480%5E%5E0&_callback=Porto_6500_1478585439295',
			]
		],
		
		'main'=> [
			'url'=>'https://passport.suning.com/ids/login',
			'post'=>true,
			'data'=>'jsonViewType=true&username={$mobile}&password=&password2=XX%2BCV1yBHiKqOT1lGsJ60HOTQnMTHtboZuMMMDvsIkgM8Cm4uNmGQxfCjjI2A13sJXndE2LXGpgGRShMc7omNjgbC3cGPws5%2F4NdZqSnhgbzTLOqgo6CCXSR0rGXocw3eYez2cwRe%2BOeHGjcSE2gFHUgBNSdQIRCbXZNVqghucw%3D&loginTheme=b2c&service=https%3A%2F%2Fssl.suning.com%2Fwebapp%2Fwcs%2Fstores%2Fauth%3FtargetUrl%3Dhttp%253A%252F%252Fwww.suning.com%252F&rememberMe=true&client=app',
		]
	],
	*/
	
	
	//?
	/* 太慢
	'jdair'=>[
		'parser'=>['json', 'register', ['unVerifyPhone', 'success', false]],
		'desc'=>'首都航空',
		
		'main'=> [
			'url'=>'http://www.jdair.net/frontend/users/register/GetMobile.action?flag={$mobile}',
			'data'=>'flag={$mobile}',
			'post'=>true,
		]
	],
	*/

	/*
	'vancl'=>[
		'parser'=>['json', 'register', ['ErrorType', 1, false]],
		'desc'=>'凡客',
		
		'list'=>[
			[
				'url'=>'http://login.vancl.com/login/Login.aspx',
				'data'=>'',
			]
		],

		'main'=> [
			'url'=>'http://login.vancl.com/login/XmlCheckUserName.ashx',
			'data'=>'Loginasync=true&LoginUserName={$mobile}&UserPassword=12345678&Validate=&type=web',
			'post'=>true,
		]
	],
	*/
	
];