<?php

return [
	'zhaopin'=>[
		'parser'=>['json', 'register', ['status', 0]],
		'desc'=>'智联招聘',
		
		'main'=> [
			'url'=>'https://passport.zhaopin.com/account/checkregister',
			'data'=>'RegisterName={$mobile}&registername={$mobile}',
			'post'=>true
		]
	],
	
	'dajie'=>[
		'parser'=>['json', 'register', ['msg', "AUTHED"]],
		'desc'=>'大街',
		
		'main'=> [
			'url'=>'http://www.dajie.com/account/phonestatuscheck',
			'data'=>'callback=cb&ajax=1&phoneNumber={$mobile}&_=1478527060152&_CSRFToken=',
			'mobile'=>false,
		]
	],
	
	'chinahr'=>[
		'parser'=>['json', 'register', ['isSuccess', true]],
		'desc'=>'中华英才网',
		
		'main'=> [
			'url'=>'http://passport.chinahr.com/ajax/m/existLoginName',
			'data'=>'input={$mobile}&_=1478573685691',
		]
	],
	
	'51job'=>[
		'parser'=>['string', 'register', ['str', "1"]],
		'desc'=>'前程无忧',
		
		'main'=> [
			'url'=>'http://my.51job.com/my/in/checkinfo.php',
			'data'=>'type=mobilePhone&value={$mobile}',
			'post'=>true
		]
	],
	
	'vko'=>[
		'parser'=>['string', 'register', ['str', "cb(false)"]],
		'desc'=>'微课',
		
		'main'=> [
			'url'=>'http://www.vko.cn/isLoginNameCanUse.html',
			'data'=>'callback=cb&loginName={$mobile}&_=1478501479415',
		]
	],
	
	'xuetangx'=>[
		'parser'=>['json', 'register', ['success', false]],
		'desc'=>'学堂在线',
		
		'main'=> [
			'url'=>'http://www.xuetangx.com/check_register_params_v2',
			'data'=>'email=&phone_number={$mobile}&username=',
			'post'=>true,
		]
	],
	
	'yiqixie'=>[
		'parser'=>['json', 'register', ['STATUS', "1"]],
		'desc'=>'一起写',
		
		'main'=> [
			'url'=>'https://yiqixie.com/e/useradmin/check/',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],
	
	'jikexueyuan'=>[
		'parser'=>['json', 'register', ['msg', "手机已使用"]],
		'desc'=>'极客学院',
		
		'main'=> [
			'url'=>'http://passport.jikexueyuan.com/check/phone?is_ajax=1',
			'data'=>'phone={$mobile}',
			'post'=>true,
		]
	],
	
	'imooc'=>[
		'parser'=>['json', 'register', ['status', "90010"]],
		'desc'=>'慕课网',
		
		'main'=> [
			'url'=>'http://www.imooc.com/passport/user/checkphone',
			'data'=>'phone={$mobile}',
		]
	],
	
	'e189'=>[
		'parser'=>['json', 'register', ['msg', '用户名已占用']],
		'desc'=>'天翼账号',
		
		'main'=> [
			'url'=>'https://e.189.cn/validation/userNameExist.do',
			'data'=>'userName={$mobile}',
		]
	],
	
	'kuaizhan'=>[
		'parser'=>['str', 'register', ['regexp', "#已经被注册#"]],
		'desc'=>'搜狐快站',
		
		'main'=> [
			'url'=>'http://www.kuaizhan.com/passport/ajax-phone-exists',
			'data'=>'phone={$mobile}',
		]
	],
	
	'51credit'=>[
		'parser'=>['str', 'register', ['str', 'false']],
		'desc'=>'我爱卡',
		
		'main'=> [
			'url'=>'https://sso.51credit.com/checkExist',
			'data'=>'newMobile={$mobile}',
			'post'=>true
		]
	],
	
];