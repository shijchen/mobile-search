<?php
/**
 * 旅游，航空，班车等出行相关
 */

return [
	'tuniu'=>[
		'parser'=>['json', 'register', ['errno', -1]],
		'desc'=>'途牛',
		
		'main'=> [
			'url'=>'https://passport.tuniu.com/register/isPhoneAvailable',
			'post'=>true,
			'data'=>'intlCode=0086&tel={$mobile}',
		]
	],
	
	'qunar'=>[
		'parser'=>['json', 'register', ['errCode', 11009]],
		'desc'=>'去哪儿',

		'main'=> [
			'url'=>'https://user.qunar.com/ajax/validator.jsp',
			'data'=>'method={$mobile}&prenum=86&vcode=null',
			'post'=>true,
		]
	],
	
	'517best'=>[
		'parser'=>['json', 'register', ['Result', '1']],
		'desc'=>'旅游百事通',
		
		'main'=> [
			'url'=>'http://member.517best.com/Handler/HandlerMember.aspx',
			'data'=>'r=0.014217926816978865&action=user_reg_check_phone&UserMobile={$mobile}&callback=cb&_=1479181994622',
			'mobile'=>false
		]
	],
	
	'lvmama'=>[
		'parser'=>['json', 'register', ['success', false]],
		'desc'=>'驴妈妈旅游',

		'main'=> [
			'url'=>'http://login.lvmama.com/nsso/ajax/checkUniqueField.do',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'ly'=>[
		'parser'=>['json', 'register', ['state', '300']],
		'desc'=>'同程旅游',
		
		'main'=> [
			'url'=>'https://passport.ly.com/Member/MemberLoginAjax.aspx',
			'data'=>'remember=30&name={$mobile}&pass=86ae25d0bac18b2206100cd658498f4x&voiceCode=&action=login',
			'post'=>true
		]
	],
	
	'qyer'=>[
		'parser'=>['json', 'register', ['error_code', 400002]],
		'desc'=>'Q穷游',
		
		'main'=> [
			'url'=>'http://login.qyer.com/qcross/login/auth.php',
			'data'=>'action=regcheck&input=86-{$mobile}&type=phone&timer=1478525412884',
		]
	],
	
	'uzai'=>[
		'parser'=>['string', 'register', ['regexp', "#status\:1\,msg#"]],
		'desc'=>'悠哉网',
		
		'main'=> [
			'url'=>'https://u.uzai.com/reg/Validation',
			'data'=>'callback=cb&clientid=txtPhoneMail&txtPhoneMail={$mobile}&ValidateToken=&_=1479182279146',
		]
	],
	
	'changtu'=>[
		'parser'=>['json', 'register', ['verifyFlag', 'true']],
		'desc'=>'畅途网',
		
		'main'=> [
			'url'=>'https://passport.changtu.com/reg/getMobileType.htm',
			'data'=>'mobile={$mobile}&channelId=5',
		]
	],
	
	'cncn'=>[
		'parser'=>['string', 'register', ['regexp', "#此手机号已被注册#"]],
		'desc'=>'欣欣旅游',
		
		'main'=> [
			'url'=>'http://www.cncn.com/reg_cncn.php',
			'data'=>'inajax=1&action=checkmobile&mobile={$mobile}',
		]
	],
	
	'caissa'=>[
		'parser'=>['string', 'register', ['regexp', "#登录#"]],
		'desc'=>'凯撒旅游',
		
		'main'=> [
			'url'=>'http://my.caissa.com.cn/Registered/CheckPhone',
			'data'=>'phone={$mobile}&imgcode=&captcha=&password=',
			'post'=>true
		]
	],
	
	'shijiebang'=>[
		'parser'=>['json', 'register', ['code', 7]],
		'desc'=>'世界邦',
		
		'main'=> [
			'url'=>'http://www.shijiebang.com/a/checker/mobile/',
			'data'=>'mobile={$mobile}',
			'post'=>true
		]
	],
	
	'cits'=>[
		'parser'=>['string', 'register', ['str', 1]],
		'desc'=>'中国国旅',
		
		'main'=> [
			'url'=>'http://www.cits.cn/regist.html',
			'data'=>'mode=isMobile&mobile={$mobile}',
			'post'=>true
		]
	],
	
	'aoyou'=>[
		'parser'=>['json', 'register', ['exist', true]],
		'desc'=>'中青旅遨游',
		
		'main'=> [
			'url'=>'https://passport.aoyou.com/Reg/VerificationMobile',
			'data'=>'regMobile={$mobile}',
			'post'=>true
		]
	],
	
	// 航空公司
	'tianxun'=>[
		'parser'=>['json', 'register', ['status', 'n']],
		'desc'=>'天巡',
		
		'main'=> [
			'url'=>'https://secure.tianxun.com/member/ajaxService.php?action=regCheck',
			'data'=>'name=mobile&param={$mobile}',
			'post'=>true,
		]
	],
	
	'tianjinair'=>[
		'parser'=>['str', 'register', ['regexp', "#被占用#"]],
		'desc'=>'天津航空',
		
		'main'=> [
		
			'url'=>'http://www.tianjin-air.com/um/userInfo/uniCheckByMobile',
			'data'=>'mobile={$mobile}',
			'post'=>true,
		]
	],
	
	'chinaexpressair'=>[
		'parser'=>['json', 'register', ['msg', 'no']],
		'desc'=>'华夏航空',
		
		'main'=> [
			'url'=>'http://www.chinaexpressair.com/hxhklandr.ac?reqCode=isAccount&Type=mobile&clientid=mobile&rand=1478155069706&mobile={$mobile}',
			'data'=>'mobile=',
			'post'=>true,
		]
	],
	
	'airchina'=>[
		'parser'=>['string', 'register', ['str', 1]],
		'desc'=>'中国国航',

		'main'=> [
			'url'=>'http://www.airchina.com.cn/www/servlet/com.ace.um.userRegister.servlet.PhoneValidator',
			'data'=>'shouji={$mobile}',
			'post'=>true,
		]
	],
	
	'hnair'=>[
		'parser'=>['json', 'register', ['error.Code', 1024]],
		'desc'=>'海南航空',
		
		'list'=>[
			[
				'url'=>'http://new.hnair.com/hainanair/ibe/profile/createProfile.do',
				'data'=>''
			]
		],
		
		'main'=> [
			'url'=>'http://new.hnair.com/hainanair/ibe/profile/processLogin.do',
			'data'=>'ConversationID=&Mobile={$mobile}&methodName=checkB2CUserById',
		]
	],
	
	'shenzhenair'=>[
		'parser'=>['json', 'register', ['regFlag', 1]],
		'desc'=>'深圳航空',
		
		'main'=> [
			'url'=>'http://ecfan.shenzhenair.com/operate/getRegister',
			'data'=>'loginMobile={$mobile}',
		]
	],
	
	'didi'=>[
		'parser'=>['json', 'register', ['data', '{"statusCode":2101,"couponList":[],"isNew":0}']],
		'desc'=>'滴滴乘客',
		
		'main'=> [
			'url'=>'https://dorado.xiaojukeji.com/api/recommend/receiveCoupon',
			'data'=>'fctoken=eed04400e7fccb52874ed1c5f170c3b5&timestamp=1478010626&recommend_mobile=MTU4MDExOTAwNzQ%3D&activity_id=9977&c=20000&b=20000&shareid=147801062664154534&d=0&isIntel=true&bind_activityID=9977&p=100&bind_cityId=1&cityId=1&app=other&receive_mobile={$base64mobile}',
		]
	],
	
	'10101111com'=>[
		'parser'=>['json', 'register', ['result.status', 3]],
		'desc'=>'神州专车',
		
		'main'=> [
			'url'=>'http://m.10101111.com/shareRedpacket/getRedpacket.do',
			'data'=>'key=newMember%23{$mobile}%23bUtvbVpzeVk4eFFMcWdIdWxwNmN6QT09%231478053873935',
			'post'=>true,
		]
	],
	
	'uber'=>[
		'parser'=>['json', 'register', ['invalid_request_fields.0.field_type', 'mobile']],
		'desc'=>'Uber乘客',
		
		'main'=> [
			'url'=>'https://get.uber.com.cn/validate_field/',
			'data'=>'field=mobile&mobile={$mobile}&mobile_country_iso2=CN',
			'post'=>true,
		]
	],
	
	'zuche'=>[
		'parser'=>['json', 'register', ['returnFlag', 'pwdError']],
		'desc'=>'神州租车',
		
		'main'=> [
			'url'=>'https://passport.zuche.com/memberManage/member.do',
			'data'=>'xname={$mobile}&xpassword=ff{$millisecond}&xyzm=%E8%AF%B7%E8%BE%93%E5%85%A5%E5%8F%B3%E4%BE%A7%E9%AA%8C%E8%AF%81%E7%A0%81&xidtm=%E8%AF%B7%E8%BE%93%E5%85%A5%E5%8A%A8%E6%80%81%E9%AA%8C%E8%AF%81%E7%A0%81&flag=&autoLogin=checked&type=normal',
			'post'=>true,
		]
	],
];