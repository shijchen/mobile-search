<?php
/**
 * 影视，动漫，小说，音乐等媒体相关
 */

return [
	'kankan'=>[
		'parser'=>['json', 'register', ['binded', 1]],
		'desc'=>'响巢看看',
		
		'main'=> [
			'url'=>'https://zhuce.kankan.com/regapi',
			'data'=>'callback=cb&op=checkBind&account={$mobile}&type=1&response=jsonp',
		]
	],
	
	'iqiyi'=>[
		'parser'=>['json', 'register', ['data', true]],
		'desc'=>'爱奇艺',
		
		'main'=> [
			'url'=>'http://passport.iqiyi.com/apis/user/check_account.action?t=1477622113314',
			'post'=>true,
			'data'=>'account={$mobile}&agenttype=1',
		]
	],
	
	'17k'=>[
		'parser'=>['json', 'register', ['error_code', 1]],
		'desc'=>'17K小说',
		
		'main'=> [
			'url'=>'http://passport.17k.com/check',
			'post'=>true,
			'data'=>'name={$mobile}&type=mobile',
		]
	],
	
	'youku'=>[
		'parser'=>['json', 'register', ['msg.exist', true]],
		'desc'=>'优酷土豆',
	
		'match'=> [
			'url'=> 'https://account.youku.com/registerView.htm',
			'data'=> 'callback=&buid=youku&template=tempA&regModel=mobile&size=normal',
			'attributes'=> ['formtoken'=>"#,\"formtoken\"\:\"(.*?)\"#"],
		],
		
		'main'=> [
			'url'=>'https://account.youku.com/passport/isUserExist.json',
			'data'=>'buid=youku&passport={$mobile}&formtoken={$formtoken}&template=tempA&bizType=register&region=CN&jsonpCallback=a',
		]
	],
	
	'pptv'=>[
		'parser'=>['string', 'register', ['str', 'pplive_callback_0(1);']],
		'desc'=>'pptv聚力',
		
		'main'=> [
			'url'=>'http://passport.pptv.com/isPhoneExist.do',
			'data'=>'phone={$mobile}&from=web&version=1.0.0&format=jsonp&cb=pplive_callback_0',
		]
	],
	
	'baofeng'=>[
		'parser'=>['json', 'register', ['info.exist', true]],
		'desc'=>'暴风影音',
		
		'main'=> [
			'url'=>'http://sso.baofeng.net/api/check/mobile',
			'data'=>'mobile={$mobile}&randnum=914.1312049765986',
		]
	],
	
	'kugou'=>[
		'parser'=>['json', 'register', ['data', 1]],
		'desc'=>'酷狗音乐',
		
		'main'=> [
			'url'=>'https://reg-user.kugou.com/v2/check/',
			'data'=>'appid=1014&type=2&username={$mobile}',
		]
	],
	
	'momentcamofficial'=>[
		'parser'=>['json', 'register', ['d', '{"StatusCode":113011,"Description":"用户名已存在"}']],
		'desc'=>'魔漫',
		
		'main'=> [
			'url'=>'http://www.momentcamofficial.com/Artist/Register.aspx/CheckUserName',
			'data'=>'{userName:"{$mobile}"}',
			'http_header'=>['Content-Type:application/json; charset=UTF-8'],
			'post'=>true,
		]
	],
	
	'acfun'=>[
		'parser'=>['string', 'register', ['str', 'false']],
		'desc'=>'AcFun',
		
		'main'=> [
			'url'=>'http://www.acfun.tv/mobileUnique.aspx',
			'data'=>'mobile={$mobile}',
		]
	],
	
	'u17'=>[
		'parser'=>['json', 'register', ['code', '-1101']],
		'desc'=>'有妖气',
		
		'main'=> [
			'url'=>'http://passport.u17.com/passport/ajax.php',
			'data'=>'mod=member_v2&act=check_mobile&signup_username={$mobile}',
		]
	],
	
	'mtime'=>[
		'parser'=>['json', 'register', ['value', 1]],
		'desc'=>'时光网',
		
		'main'=> [
			'url'=>'http://passport.mtime.com/Services/UserService.p',
			'data'=>'Ajax_CallBack=true&t=201611718295927256&Ajax_CallBackType=Mtime.Passport.Pages.UserService&Ajax_CallBackMethod=UserResisterStatus&Ajax_RequestUrl=http%3A%2F%2Fpassport.mtime.com%2Fmember%2Fsignin%2F&Ajax_CallBackArgument0={$mobile}',
		]
	],
	
	'1905com'=>[
		'parser'=>['json', 'register', ['status', 200]],
		'desc'=>'1905电影',
		
		'main'=> [
			'url'=>'http://passport.1905.com/v2/api/',
			'data'=>'callback=cb&m=user&a=checkEmailOrmobile&format=json&emailormobile={$mobile}&_=1478514785989',
		]
	],
	
	'yy'=>[
		'parser'=>['json', 'register', ['code', 3]],
		'desc'=>'YY/虎牙',
		
		'list'=>[
			[
				'url'=>'https://zc.yy.com/reg/udb/reg4udb.do',
				'data'=>'mode=udb&type=Mobile&appid=1&foreignMb=1&action=3&fromadv=lgn&reqDomainList='
			]
		],
		
		'main'=> [
			'url'=>'https://zc.yy.com/reg/pc/chk.do',
			'post'=>true,
			'data'=>'passport={$mobile}&mobilefix=&appid=1',
		]
	],
	
	'ku6'=>[
		'parser'=>['json', 'register', ['status', 0]],
		'desc'=>'酷6网',
		
		'main'=> [
			'url'=>'http://passport.ku6.com/v3-ifreg.htm',
			'data'=>'loginNamePar={$mobile}',
		]
	],
];