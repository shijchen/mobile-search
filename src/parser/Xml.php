<?php
/**
 * 通用XML解析器
 */
namespace mobile\parser;

class Xml extends Base{
	public function register($conf) {
		$data = @ simplexml_load_string(trim($this->data), 'SimpleXMLElement', LIBXML_NOCDATA|LIBXML_NOBLANKS);
		
		if(empty($data))
			return $this->status['failed'];
		
		if(defined('TESTER') && TESTER) print_r($data);
		
		$res = $this->findValue((array)$data, $conf[0]);
		
		return $this->result($res, $conf);
	}
}