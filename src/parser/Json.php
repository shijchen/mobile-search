<?php
/**
 * 通用json解析器
 */
namespace mobile\parser;

class Json extends Base{
	public function parseJson() {
		$result = preg_replace("#^.*?\{(.*?)[^\}]*$#", '{$1', $this->data);
		$resultArr = @ json_decode($result, true);
		if(defined('TESTER') && TESTER) var_dump($resultArr);
		return $resultArr ? $resultArr : array();
	}
	
	public function register($conf) {
		$data = $this->parseJson();
		if(empty($data))
			return $this->status['failed'];
		
		$res = $this->findValue($data, $conf[0]);
		
		return $this->result($res, $conf);
	}
}