<?php
/**
 * 通用json解析器
 */
namespace mobile\parser;

class Str extends Base{
	public function register($conf) {
		if($this->data === '')
			return $this->status['failed'];
		
		$type = isset($conf[2]) ? $conf[2] : true;
		
		switch($conf[0]) {
			case 'str':
				$state = $type === true ? $this->data == $conf[1] : $this->data != $conf[1];
				break;
			case 'regexp':
				$state = preg_match($conf[1], $this->data);
				
				$type === false && $state = !$state;
				break;
			default:
				break;
		}
		
		return $this->status[$state ? 'reg' : 'unreg'];
	}
}