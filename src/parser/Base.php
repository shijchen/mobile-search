<?php

namespace mobile\parser;

class Base{
	protected $data;
	
	protected $status = [
		'failed'=>[3, '查询失败'],
		'unreg'=>[2, '未注册'],
		'reg'=>[1, '已注册'],
	];
	
	public function __construct($data) {
		!mb_check_encoding($data, 'utf8') && $data = mb_convert_encoding($data, 'utf8', 'gbk');
		$this->data = $data;
		
		if(defined('TESTER') && TESTER) var_dump($data);
	}
	
	/**
	 * 从数组中查询一个值
	 * $data 被查询的数组
	 * $path 示例 'a.b', 查询 是否有  $data['a']['b']
	 */
	public function findValue($data, $path) {
		$arr = explode('.', $path);
		foreach($arr as $key) {
			if( !is_array($data) || !isset($data[$key]) ) {
				$data = null;
				break;
			}
			
			$data = $data[$key];
		}
		
		return $data;
	}
	
	/**
	 * 结果计算
	 */
	public function result($res, $conf) {
		$type = isset($conf[2]) ? $conf[2] : true;
		$state = $type === true ? $res == $conf[1] : $res != $conf[1];
		return $this->status[$state ? 'reg' : 'unreg'];
	}
}
