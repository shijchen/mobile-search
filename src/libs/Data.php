<?php
namespace mobile\libs;
/*
 * 数据配置
 */
class Data {
	/**
	 * 获取元数据，self::datas 和 datas 目录下的php文件
	 */
	public static function getDatas() {
		$datas = array();
		$dir = DIR.'/src/datas/';
		
		if($dh = opendir($dir)) {
			while(($file = readdir($dh)) !== false) {
				$file = $dir.$file;
				if(!is_file($file) || !preg_match("#\.php$#", $file)) continue;
				
				$datas = array_merge($datas, include $file);
			}
			closedir($dh);
		}
		
		return $datas;
	}
}