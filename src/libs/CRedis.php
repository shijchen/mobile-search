<?php
namespace mobile\libs;

class CRedis{
	public static function model() {
		$redis = new \Redis();
		$redis->connect('127.0.0.1', 6379, 30);
		$redis->setOption(\Redis::OPT_SERIALIZER, 1);
		
		$redis->select(1);
		
		return $redis;
	}
}
