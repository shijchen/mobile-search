<?php
namespace mobile;
use mobile\libs\Helper;
use mobile\libs\Request;
use mobile\libs\CRedis;
use mobile\parser as parser;

class Boot {
	protected $datas = array();
	
	protected $redis;
	
	public function __construct($datas) {
		$this->datas = $datas;
	}
	
	// 运行
	public function run() {
		foreach($this->datas as $key => $item) {
			if(empty($item)) continue;
			TESTER && printf("Begin Request: %s \n", $item['desc']);
			
			$res = Request::run($item);
			$this->logResult($item, $key, $res);
		}
		
		Helper::clear();
	}
	
	/**
	 * 记录查询日志
	 */
	protected function logResult($item, $key, $res) {
		@ list($class, $method, $args) = $item['parser'];
		if($class == 'string') $class = 'str';
		$class = 'mobile\\parser\\'.ucfirst($class);
		
		$res = ( new $class($res) )->{$method}($args);
		
		if(TRY_IT) {
			
			if($res[0] != 1) echo MOBILE, ' - ', $item['desc'], ' - ', $key, "\n";
			
		} else {
			
			if(TESTER || $res[0] == 1)
				echo MOBILE, " > ", $res[1], "\n";
			
		}
		
		$res[1] = $item['desc'].': '.$res[1];
	}
}

class AutoLoad {
	public static function load($className) {
		$className = str_replace('\\', '/', substr($className, 6));
		
		$filename = DIR.'/src/'.$className.'.php';
		
		if(is_file($filename)) {
			include($filename);
			return true;
		}
		
		return false;
	}
}

spl_autoload_register(array('mobile\AutoLoad','load'));
