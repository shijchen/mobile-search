<?php
use mobile\Boot;
use mobile\libs\Helper;
use mobile\libs\Data;
use mobile\libs\Curl;

date_default_timezone_set('Asia/Shanghai');

if(function_exists('fastcgi_finish_request')) {
	exit('End');
}

define('DIR', __DIR__);

require __DIR__.'/src/Boot-test.php';

$args = Helper::parseArgv($argv);

define('LOCAL', true);
define('TESTER', isset($args['nodubug']) && $args['nodubug'] ? FALSE : TRUE);
define('TRY_IT', isset($args['try']) && $args['try'] ? TRUE : FALSE);

if(isset($args['line']) && !empty($args['line'])) {
	$arr = explode('|', $args['line']);
	$args['mobile'] = $arr[1];
	$key = $arr[0];
} else {
	
	$key = 'p2p001';
}

if(!isset($args['mobile']) || !preg_match("#^1\d{10}$#", $args['mobile'])) {
	echo "Usage: \nphp index.php --mobile=11111111111 \n";
	exit();
}

define('MOBILE', $args['mobile']);

$data = Data::getDatas();

if(!isset($data[$key])) {
	echo "No";
	exit();
}

$boot = new Boot([$data[$key]]);
//$boot = new Boot($data);
$boot->run();